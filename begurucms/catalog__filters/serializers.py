from rest_framework import serializers
from begurucms.catalog.models import Product
from .models import Filters, FilterData


class ProductArtConfiguratorJSONSerializer(serializers.Serializer):
    """ Product serializer for configurators """
    art = serializers.CharField(max_length=256)
    title = serializers.CharField(max_length=512)


class ProductConfiguratorJSONSerializer(serializers.ModelSerializer):
    """ Product serializer to JSON data """
    class Meta:
        model = Product
        fields = ('art', 'title', 'desc', 'icon')
