from django.apps import AppConfig


class CatalogFiltersConfig(AppConfig):
    name = 'catalog__filters'
