from django.contrib import admin
from begurucms.catalog.models import Char
from .models import ProductConfigurator


@admin.register(ProductConfigurator)
class ProductConfiguratorAdmin(admin.ModelAdmin):
    """
    Choice sections to configurator
    """
    list_display = ('name', 'ident')

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """
        Only chars into sections and sub sections in ProductConfigurator
        """
        if db_field.name == 'chars':
            if not request.resolver_match.args:
                kwargs["queryset"] = Char.objects.none()
            else:
                sections = ProductConfigurator.objects.get(
                    pk=request.resolver_match.args[0]
                ).sections.all().get_descendants(include_self=True)
                kwargs["queryset"] = Char.objects.filter(
                    productchar__product__sections__in=sections
                ).distinct().order_by('name')
        return super(ProductConfiguratorAdmin, self).formfield_for_manytomany(
            db_field,
            request,
            **kwargs
        )
