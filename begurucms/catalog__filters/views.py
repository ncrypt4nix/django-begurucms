from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework import mixins, generics
from rest_framework.views import Http404
from rest_framework.renderers import JSONRenderer
from begurucms.catalog.models import Product, ProductChar
from .models import ProductConfigurator
from .serializers import (
    ProductArtConfiguratorJSONSerializer,
    ProductConfiguratorJSONSerializer
)


class GetProductConfiguratorJSON(generics.GenericAPIView):
    """ get product data json """

    serializer_class = ProductConfiguratorJSONSerializer

    def get(self, request, *args, **kwargs):
        if 'art' not in request.GET:
            raise Http404()
        product = get_object_or_404(Product, art=request.GET['art'])
        return Response(self.serializer_class(product).data)


class FilteredProductConfiguratorJSONVIew(
    mixins.ListModelMixin,
    generics.GenericAPIView
):
    """ json view for filter product """

    serializer_class = ProductArtConfiguratorJSONSerializer
    TEMPLATE_VALUE_KEY_ALL = 'Все'

    def get_queryset(self, *args, **kwargs):
        """ get filtered products queryset """

        configurator = get_object_or_404(
            ProductConfigurator,
            ident=self.kwargs.get('configurator_name')
        )
        sections = configurator.sections.all().get_descendants(
            include_self=True
        )
        filtered_objs = (
            lambda json_data=self.request.GET,
            all=FilteredProductConfiguratorJSONVIew.TEMPLATE_VALUE_KEY_ALL:
            [
                {i: json_data[i]}
                for i in json_data
                if json_data[i] != all
            ]
        )()
        products = Product.objects.filter(
            productchar__char__in=configurator.chars.all(),
            sections__in=sections
        ).distinct()
        if 'product_art' in self.kwargs:
            product = get_object_or_404(
                Product,
                art=self.kwargs.get('product_art')
            )
            products = products.filter(productrelated__product=product)
        for item in filtered_objs:
            value = item.popitem()
            products = products.filter(
                productchar__char__name=value[0],
                productchar__value=value[1]
            )
        return products.values('art', 'title')

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ProductConfiguratorView(TemplateView):
    """ template configurator products from ProductConfigurator model or
        configurator subproduct from product.
    """
    template_name = 'catalog__filters/product_configurator.tpl'

    def get_context_data(self, configurator_name, product_art=None, **kwargs):
        """ get product articles and characteristics """

        context = super().get_context_data(**kwargs)
        configurator = get_object_or_404(
            ProductConfigurator,
            ident=configurator_name
        )
        context['configurator'] = configurator
        context['sub_configurator'] = configurator.sub_configurator
        sections = configurator.sections.all().get_descendants(
            include_self=True
        )
        data = ProductChar.objects.filter(
            char__in=configurator.chars.all(),
            product__sections__in=sections
        ).only(
            'value', 'char__name', 'product__art', 'product__title'
        ).select_related(
            'char__name', 'product__art', 'product__title'
        ).distinct()
        if product_art:
            product = get_object_or_404(Product, art=product_art)
            data = data.filter(product__productrelated__product=product)
            context['product_art'] = product.art
        data_values = data.values('value', 'char__name')
        new_data = [
            [i, {}]
            for i in set(
                [
                    i['char__name']
                    for i in data_values
                ]
            )
        ]
        for i in new_data:
            i[1] = set(
                (
                    lambda data=data_values, char_name=i[0]:
                    [
                        i['value']
                        for i in data
                        if i['char__name'] == char_name
                    ]
                )()
            )
        context['data'] = new_data
        arts = [
            {
                'art': i['product__art'],
                'title': i['product__title']
            }
            for i in data.values('product__art', 'product__title')
        ]
        products = ProductArtConfiguratorJSONSerializer(arts, many=True)
        context['products'] = JSONRenderer().render(products.data)
        return context
