from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.validators import validate_slug


class ProductConfigurator(models.Model):
    """ Product configuration into sections """
    name = models.CharField(max_length=255)
    sections = models.ManyToManyField(to='catalog.SectionMPTT')
    ident = models.CharField(
        max_length=128,
        help_text=_(
            'Identifier for URL. Use letters, numbers, and underscores'
        ),
        validators=[validate_slug],
        unique=True,
    )
    chars = models.ManyToManyField(to='catalog.Char', blank=True)
    sub_configurator = models.ForeignKey(
        'self',
        help_text=_(
            'Sub configurators. Like example: cupboard -> accessory'
        ),
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = _("Product Configurator")
        verbose_name_plural = _("Product Configurators")

    def __str__(self):
        return self.name
