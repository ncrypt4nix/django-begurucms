from captcha.fields import ReCaptchaField
from django import forms
from .models import UnauthorizedBucket


class UnauthorizedBucketForm(forms.ModelForm):
    """Model form to Unauthorized users"""
    captcha = ReCaptchaField()

    def __init__(self, *args, **kwargs):
        kwargs.update({'label_suffix': ''})
        super().__init__(*args, **kwargs)

    class Meta:
        model = UnauthorizedBucket
        exclude = ('bucket', 'updated')
