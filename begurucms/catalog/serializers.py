from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from .models import (
    SectionMPTT,
    BucketItem,
    SubProduct
)


class BucketItemProductSerializer(serializers.ModelSerializer):
    """Serializer to bucket item"""
    art = serializers.SerializerMethodField(read_only=True)
    url = serializers.CharField(
        read_only=True,
        source='get_real_product.get_absolute_url'
    )
    real_product_art = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = BucketItem
        fields = ('icon', 'annotation', 'art', 'url', 'real_product_art')

    def get_art(self, obj):
        return obj.product.art

    def get_real_product_art(self, obj):
        return obj.get_real_product().art


class BucketItemSerializer(serializers.ModelSerializer):
    """Serializer to bucket item"""
    product = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = BucketItem
        fields = ('product', 'count')

    def get_product(self, obj):
        real = obj.get_real_product()
        return {
            'art': obj.product.art,
            'icon': obj.icon,
            'annotation': obj.annotation,
            'url': real.get_absolute_url(),
            'unit': real.unit if real.unit else _('pc'),
            'real_product_art': real.art,
        }


class SubProductToBucketSerializer(serializers.ModelSerializer):
    """Serializer to subproduct with order table"""
    ordering_table = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = SubProduct
        fields = ('art', 'ordering_table')

    def get_ordering_table(self, obj):
        """get dynamic sub_product fields"""
        table = dict()
        values = obj.list_table()
        keys = [
            i.get('main')
            for i in obj.product.list_table().get('content')
        ]
        for i in range(0, len(keys)):
            try:
                table[keys[i]] = values[i]
            except IndexError:
                return table
        return table


class CompareSectionSerializer(serializers.ModelSerializer):
    """section serializer to compare page"""
    count = serializers.SerializerMethodField()

    class Meta:
        model = SectionMPTT
        fields = ('id', 'title', 'count')

    def get_count(self, obj):
        session = self.context['request'].session
        try:
            return len(session['compare_sections'][obj.id])
        except KeyError:
            return 0
