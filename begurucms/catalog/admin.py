# coding=utf-8
import os
import csv
import re
import difflib
import threading
import urllib.request
from xlrd import open_workbook, XLRDError
from collections import deque

from django.core.files import File
from django.core.urlresolvers import reverse
from django.contrib import admin, messages
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe
from django.forms import TextInput
from django.forms.models import BaseInlineFormSet
from django.forms import ValidationError
from django.db import transaction, models
from mptt.admin import DraggableMPTTAdmin
from begurucms.utils.admin import GenericAbstractAdmin
from begurucms.pages.admin import (
    PublicationContentFiles,
    ContentFilesInline,
    ObjectDynamicParamStackedInline,
    TaggitTabularInline
)
from begurucms.pages.models import cfile_upload_to
from begurucms.core.admin import rebuild_MPTT
from begurucms.useful_resources.models import Resource

from .forms import RequiredFormSet
from .models import (
    ProductChar,
    SubProduct,
    OrderTableItem,
    ProductBasicConfiguration,
    ProductRelated,
    Product,
    ProductAdditionalInformation,
    KeyFeatures,
    ProductImage,
    SectionMPTT,
    ProductSections,
    PriceType,
    ProductPrice,
    PriceList,
    Vendor,
    ProductRelatedType,
    UnauthorizedBucket,
    Char
)


def move_old_table_to_new(queryset):
    """
        queryset: class 'begurucms.catalog.models.SubProduct'
        return 2 values (errors, error_chars)
    """
    errors = list()
    error_chars = list()
    for i in queryset:
        ordering_table = deque()
        keys = list()
        for old_key in i.product.list_table().get('content'):
            if old_key.get('subcol'):
                keys += [
                    {'main': name, 'parent': old_key.get('main')}
                    for name in old_key.get('subcol')
                ]
                continue
            keys.append(old_key)
        values = i.list_table()
        if values and len(keys) < len(values):
            errors.append(i.art)
            continue
        historic = [
            i
            for i in OrderTableItem.objects.filter(product=i).exclude(
                char__name__in=[i['main'] for i in keys]
            )
        ]
        for key in keys:
            try:
                char, _ = Char.objects.get_or_create(name=key.get('main'))
            except Char.MultipleObjectsReturned:
                char = Char.objects.filter(name=key.get('main'))[0]
                error_chars.append(char.name)
            try:
                item = OrderTableItem.objects.get(char=char, product=i)
            except OrderTableItem.DoesNotExist:
                if not historic:
                    item = OrderTableItem(char=char, product=i)
                else:
                    item = historic.pop()
                    item.value = None
                item.changed = True
            except OrderTableItem.MultipleObjectsReturned:
                item = OrderTableItem.objects.filter(
                    char=char,
                    product=i
                ).first()
            if key.get('parent'):
                parent_char, _ = Char.objects.get_or_create(
                    name=key.get('parent')
                )
                parent, _ = OrderTableItem.objects.get_or_create(
                    char=parent_char,
                    product=i
                )
                item.parent = parent
                item.changed = True
            ordering_table.append(item)
        for value in values:
            item = ordering_table.popleft()
            item.value = value
            item.save()
        # create new order table item in db
        for i in [i for i in ordering_table if getattr(i, 'changed', None)]:
            i.save()
        # delete historic items
        OrderTableItem.objects.filter(id__in=[i.id for i in historic]).delete()
    return errors, error_chars


class OrderTableIteminline(admin.TabularInline):
    fields = ('char', 'value',)
    model = OrderTableItem


@admin.register(SubProduct)
class SubProductAdmin(admin.ModelAdmin):
    fields = ('product', 'art', 'code_1c', 'table')
    inlines = [OrderTableIteminline]
    actions = ['move_old_table_to_new_action']

    def move_old_table_to_new_action(self, request, queryset):
        errors, error_chars = move_old_table_to_new(queryset)
        msg = 'Finished. '
        if errors:
            msg += 'Please fix this products: {}'.format(errors)
        if error_chars:
            msg += 'This Chars is duplicated: {}'.format(error_chars)
        self.message_user(request, msg)

    move_old_table_to_new_action.short_description = _(
        'Move old ordering table to new format'
    )


@admin.register(OrderTableItem)
class OrderTableItemAdmin(DraggableMPTTAdmin):
    """
        Intermediate admin page,
        used to group items of the order table,
        to implement one header for several items of the order table
    """
    fields = ('char', 'value',)
    list_display = (
        'tree_actions',
        'indented_title',
        'product',
        'char',
        'value'
    )
    actions = [rebuild_MPTT]
    list_filter = ('product',)


class ProductCharsInlineFormset(BaseInlineFormSet):
    def clean(self):
        """
        We override the cleaning method to make empty values
        for elements that are top-level groups
        """
        super(ProductCharsInlineFormset, self).clean()
        valid_forms = [
            form for form in self.forms
            if form.is_valid() and form not in self.deleted_forms
        ]
        for form in valid_forms:
            cleaned_data = form.clean()
            if cleaned_data.get('char'):
                if (not cleaned_data.get('value') and
                        not cleaned_data['char'].has_top_group):
                    raise ValidationError(
                        _('{0} not null'.format(cleaned_data['char'].name))
                    )


@admin.register(ProductChar)
class ProductCharAdmin(DraggableMPTTAdmin):
    """MPTT Chars Admin model"""
    fields = ('char', 'value',)
    list_display = (
        'tree_actions',
        'indented_title',
        'product',
        'char',
        'value'
    )
    actions = [rebuild_MPTT]
    list_filter = ('product',)


class ProductCharsInline(admin.TabularInline):
    model = ProductChar
    formset = ProductCharsInlineFormset
    fields = ('char', 'value',)
    extra = 1
    classes = ('collapse',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'char':
            kwargs['queryset'] = Char.objects.all().order_by('name')
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class SubProductInline(admin.StackedInline):
    model = SubProduct
    fields = ('art', 'code_1c', 'table', 'ordering_table')
    readonly_fields = ('ordering_table', )
    extra = 1
    classes = ('collapse',)

    def ordering_table(self, obj):
        """template for view ordering table for sub product"""
        url = reverse(
            'admin:{}_{}_change'.format(
                obj._meta.app_label,
                obj._meta.model_name
            ),
            args=[obj.id]
        )
        tpl = '<a href="{}" target="_blank"><button type="button">Change.</button></a>'.format(url)
        tpl += '<table><thead><tr><td>Char</td><td>Value</td></tr></thead><tbody>{table}</tbody></table>'
        table = str()
        for i in obj.ordertableitem_set.all():
            table += '<tr><td>{char}</td><td>{value}</td></tr>'.format(
                char=i.char, value=i.value
            )
        return mark_safe(tpl.format(table=table))


class ProductBasicConfigurationInline(admin.TabularInline):
    model = ProductBasicConfiguration
    fk_name = 'product'
    fields = ('option', 'qty', 'pos')
    extra = 1
    classes = ('collapse',)


class ProductRelatedInline(admin.TabularInline):
    model = ProductRelated
    fk_name = 'product'
    fields = ('type', 'relateds')
    extra = 1
    classes = ('collapse',)
    filter_horizontal = ('relateds',)


class ProductInline(admin.TabularInline):
    model = Product
    filter_horizontal = ('sections',)
    fields = ('sections',)
    classes = ('collapse',)


class ProductM2MInline(admin.TabularInline):
    model = Product.sections.through
    verbose_name = _('Product')
    verbose_name_plural = _('Products')
    classes = ('collapse',)
    fieldsets = (
        (None, {'fields': ('product',)}),
    )


class ProductAdditionalInformationInline(admin.TabularInline):
    model = ProductAdditionalInformation
    fields = ('title', 'rows', 'cells')
    extra = 1
    classes = ('collapse',)


class KeyFeaturesInline(admin.TabularInline):
    model = KeyFeatures
    fields = ('icon', 'desc', )
    extra = 1
    classes = ('collapse',)


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    fields = ('image', 'title', 'position')
    extra = 1
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '80'})},
    }
    classes = ('collapse',)


class VendorAdmin(GenericAbstractAdmin):
    list_display = ('ident', 'position', 'title', 'salut', 'ispublished')
    list_editable = ('position', 'ispublished', )
    inlines = [
        TaggitTabularInline,
        ContentFilesInline,
        ObjectDynamicParamStackedInline
    ]
    summernote_fields = ('htmldesc', 'brief')
    fieldsets = (
        (None, {
            'fields': (
                'ispublished',
                'ident',
                'position',
                'title',
                'salut',
                'web',
                'email',
                'address',
                'phone',
                'brief',
                'template_name',
                'htmldesc',
                'estabilished',
                'place'
            )
        }),
        (_('Params'), {
            'classes': ('collapse',),
            'fields': ('tags', 'keywords', 'desc', 'published', 'icon', )
        }),
    ) + GenericAbstractAdmin.fieldsets


class ProductSectionsInline(admin.TabularInline):
    formset = RequiredFormSet
    model = Product.sections.through
    fields = ('sectionmptt', )
    extra = 1
    classes = ('collapse',)


class ProductAdmin(GenericAbstractAdmin):
    list_display = (
        'art',
        'title',
        'vendor',
        'position',
        'qtysections',
        'qtyimages',
        'new',
        'featured',
        'ispublished',
        'archived',
    )
    list_filter = ('ispublished', 'new', 'sections')
    list_editable = ('ispublished', 'new', 'position')
    filter_horizontal = ('sections',)
    search_fields = ('art', 'title', 'vendor__title')
    summernote_fields = ('annotation', 'content', 'desc')
    inlines = [
        ProductCharsInline,
        ProductSectionsInline
    ]

    if (hasattr(settings, 'USE_CONTENT_FILES') and
            'catalog.product.both' in settings.USE_CONTENT_FILES):
        inlines += [ContentFilesInline, ProductImageInline]
    elif (hasattr(settings, 'USE_CONTENT_FILES') and
            'catalog.product.only' in settings.USE_CONTENT_FILES):
        inlines += [ContentFilesInline]
    else:
        inlines += [ProductImageInline]

    inlines += [
        ProductRelatedInline,
        KeyFeaturesInline,
        SubProductInline,
        ProductBasicConfigurationInline,
        ProductAdditionalInformationInline
    ]

    if 'catalog__filters' in settings.INSTALLED_APPS:
        from begurucms.catalog__filters.admin import SuitableFiltersInline
        inlines.append(SuitableFiltersInline)

    actions = [
        'saveimages',
        'convertimages',
        'createdesc',
        'copy',
        'reload_images',
        'export1c',
        'import1c_code',
        'change_unit'
    ]

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)
        move_old_table_to_new(obj.subproduct_set.all())

    def import1c_code(self, request, queryset=None):
        counter = 0
        for product in queryset:
            result = product.get_or_create_nomenclature()
            if result:
                counter += 1
        self.message_user(request, "К {0} продуктам из {1} \
                          добавлен код 1C".format(counter, queryset.count()))

    import1c_code.short_description = "Импортировать 1С-код для выбранных \
        товаров"

    def export1c(self, request, queryset=None):
        products_list = Product.objects.all()
        filename = "export_links.txt"
        response = HttpResponse(content_type='text/plain')
        response['Content-Disposition'] = 'attachment; filename="{0}"'.format(
            filename
        )
        csv_resp = csv.writer(response, delimiter='#')

        for item in products_list:
            if item.subproduct_set.exists():
                for subitem in item.subproduct_set.all():
                    csv_resp.writerow([
                        subitem.art,
                        '{0}{1}'.format(
                            settings.SERVPREFIX,
                            item.get_absolute_url(
                                section=item.sections.first()
                            )
                        )
                    ])
            else:
                csv_resp.writerow([
                    item.art, '{0}{1}'.format(
                        settings.SERVPREFIX, item.get_absolute_url()
                    )
                ])
        return response
    export1c.short_description = 'Экпорт для 1с'

    def change_unit(self, request, queryset):
        """Change unit in selected queryset"""
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        return HttpResponseRedirect(
            reverse('admin_change_unit') +
            '?ids={ids}'.format(ids=",".join(selected))
        )

    def copy(self, request, queryset):
        for obj in queryset:
            changable_qs = [
                obj.productchar_set.all(),
                obj.keyfeatures_set.all(),
                obj.productsections_set.all(),
                obj.content_files.all(),
                obj.product_related.all(),
                obj.productbasicconfiguration_set.all(),
                obj.resource_set.all(),
            ]
            obj.pk = None
            obj.art = obj.art + '__copy'
            obj.save()
            for next_qs in changable_qs:
                for item in next_qs:
                    if isinstance(item, Resource):
                        item.products.add(obj)
                        continue
                    relateds = list()
                    if isinstance(item, ProductRelated):
                        relateds.extend(list(item.relateds.all()))
                    item.pk = None
                    if isinstance(item, PublicationContentFiles):
                        item.content_object = obj
                        cfile_src = item.cfile.path
                        cfile_dst = os.path.join(
                            settings.EXTERNAL_MEDIA_ROOT,
                            cfile_upload_to(
                                item,
                                os.path.basename(item.cfile.name)
                            )
                        )
                        with open(cfile_src, 'rb') as f:
                            data = f.read()
                            try:
                                os.makedirs(os.path.dirname(cfile_dst))
                            except FileExistsError:
                                pass
                            with open(cfile_dst, 'wb') as f:
                                f.write(data)
                        item_cfile = open(cfile_dst, 'rb')
                        item.cfile.file = File(item_cfile)
                        item.cfile.save(
                            os.path.basename(item.cfile.name),
                            File(item_cfile)
                        )
                    try:
                        item.save()
                        if isinstance(item, PublicationContentFiles):
                            obj.content_files.add(item)
                    except FileNotFoundError:
                        messages.error(
                            request,
                            'File not copied, file "{}" does not exist!'.format(
                                item.title
                            )
                        )
                    if relateds:
                        item.relateds.add(*relateds)
        self.message_user(request, 'Продукт скопирован!')
    copy.short_description = 'Копировать'

    def saveimages(self, request, queryset):
        umes = []
        for obj in queryset:
            icon = obj.iconresource()
            if icon:
                if icon.image.startswith('http://'):
                    filename = icon.image.split('/')[-1:][0]
                    urllib.request.urlretrieve(
                        icon.image,
                        'static/catalog/products/{0}'.format(filename)
                    )
                    obj.icon = filename
                    obj.save()
        self.message_user(request, umes)
    saveimages.short_description = "Создать иконки"

    def convertimages(self, request, queryset):
        import os
        from PIL import Image as Img
        from io import BytesIO
        issue_path = 'static/catalog/products'

        umes = []
        for obj in queryset:
            with open(os.path.join(issue_path, obj.icon), 'rb') as img:
                image = Img.open(BytesIO(img.read()))
                if (image.size[0] < 1200) and (image.size[1] < 630):
                    # if image less than target - put it to center
                    newimage = Img.new('RGBA', (1200, 630), (255, 255, 255, 0))
                    x = 600 - int(image.size[0] / 2)
                    y = 315 - int(image.size[1] / 2)
                    newimage.paste(
                        image,
                        (x, y, x + image.size[0], y + image.size[1])
                    )
                elif (image.size[0] > 1200) or (image.size[1] > 630):
                    if image.size[0] > 1200:
                        wpercent = (1200 / float(image.size[0]))
                        hsize = int((float(image.size[1]) * float(wpercent)))
                        if hsize <= 630:
                            resized = image.resize(
                                (1200, hsize),
                                Img.ANTIALIAS
                            )
                        else:
                            hpercent = (630 / float(image.size[1]))
                            wsize = int(
                                (float(image.size[0]) * float(hpercent))
                            )
                            resized = image.resize((wsize, 630), Img.ANTIALIAS)
                    else:
                        hpercent = (630 / float(image.size[1]))
                        wsize = int((float(image.size[0]) * float(hpercent)))
                        if wsize <= 1200:
                            resized = image.resize((wsize, 630), Img.ANTIALIAS)
                        else:
                            wpercent = (1200 / float(image.size[0]))
                            hsize = int(
                                (float(image.size[1]) * float(wpercent))
                            )
                            resized = image.resize(
                                (1200, hsize),
                                Img.ANTIALIAS
                            )
                    newimage = Img.new('RGBA', (1200, 630), (255, 255, 255, 0))
                    x = 600 - int(resized.size[0] / 2)
                    y = 315 - int(resized.size[1] / 2)
                    newimage.paste(resized, (x, y))
                else:
                    newimage = image

                saveimage = newimage

                output = BytesIO()
                newimage.save(output, format='PNG')
                output.seek(0)

                with open(
                    os.path.join(
                        issue_path, "{0}_1200x630.png".format(obj.art)
                    ),
                    'wb'
                ) as f:
                    f.write(output.read())
                f.close()

                with open('static/img/site_logo.png', 'rb') as img_logo:
                    image_logo = Img.open(BytesIO(img_logo.read()))
                    newimage.paste(
                        image_logo,
                        (
                            1200 - image_logo.size[0],
                            630 - image_logo.size[1], 1200, 630
                        )
                    )
                img_logo.close()

                output = BytesIO()
                newimage.save(output, format='PNG')
                output.seek(0)

                with open(
                    os.path.join(
                        issue_path, "{0}_logo_1200x630.png".format(obj.art)
                    ),
                    'wb'
                ) as f:
                    f.write(output.read())
                f.close()

                newimage = newimage.resize((600, 315), Img.ANTIALIAS)

                output = BytesIO()
                newimage.save(output, format='PNG')
                output.seek(0)

                with open(
                    os.path.join(
                        issue_path, "{0}_logo_600x315.png".format(obj.art)
                    ),
                    'wb'
                ) as f:
                    f.write(output.read())
                f.close()

                newimage = saveimage.resize((600, 315), Img.ANTIALIAS)

                output = BytesIO()
                newimage.save(output, format='PNG')
                output.seek(0)

                with open(
                    os.path.join(
                        issue_path, "{0}_600x315.png".format(obj.art)
                    ),
                    'wb'
                ) as f:
                    f.write(output.read())
                f.close()

            img.close()
            umes.append(obj.art)

        self.message_user(request, umes)
    convertimages.short_description = "Конвертировать иконки"

    def createdesc(self, request, queryset):
        umes = []
        for obj in queryset:
            if not obj.desc:
                obj.desc = obj.brief[:300]
                obj.save()
                umes.append(obj.art)

        self.message_user(request, umes)
    createdesc.short_description = "Создать дескрипшены"

    def reload_images(self, request, queryset):
        for obj in queryset:
            filename = str(obj.icon).split('/')[-1:][0]
            try:
                obj.icon.file
            except FileNotFoundError:
                if str(obj.icon).startswith('http://'):
                    picture = urllib.request.urlopen(str(obj.icon))
                    obj.icon.save(filename, picture)
                else:
                    for d, dirs, files in os.walk(
                                settings.EXTERNAL_MEDIA_ROOT
                            ):
                        for f in files:
                            if f == filename:
                                path = os.path.join(d, f)
                                dfile = File(open(path, 'rb'))
                                obj.icon.save(filename, dfile, save=True)
    reload_images.short_description = "Перезагрузить картинки"

    fieldsets = (
        (_('Main'), {
            'classes': ('collapse', ),
            'fields': (
                'ispublished',
                'in_main',
                'archived',
                'new',
                'featured',
                'unit',
                'vendor',
                'position',
                'art',
                'code_1c',
                'title',
                'annotation',
                'content',
                'benefits',
                'brief',
                'chars',
                'table',
            )
        }),
        (_('Params'), {
            'classes': ('collapse',),
            'fields': ('tags', 'keywords', 'desc')
        }),
    ) + GenericAbstractAdmin.fieldsets

    def get_form(self, request, obj=None, **kwargs):
        form = super(ProductAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['benefits'].widget.attrs['style'] = (
            'width: 80em; min-height:100px;'
        )
        form.base_fields['annotation'].widget.attrs['style'] = (
            'width: 80em; min-height:100px;'
        )
        form.base_fields['brief'].widget.attrs['style'] = (
            'width: 80em; min-height:100px;'
        )
        form.base_fields['chars'].widget.attrs['style'] = (
            'width: 80em; min-height:100px;'
        )
        form.base_fields['art'].widget.attrs['style'] = 'width: 80em;'
        return form


class PriceListAdmin(admin.ModelAdmin):
    list_display = ('name', 'vendor', 'price_type', 'ef_date')

    fieldsets = (
        (None, {
            'fields': (
                'name',
                'ef_date',
                'file',
                'brief',
                'price_type',
                'vendor'
            )
        }),
        ('Дополнительно', {
            'classes': ('collapse',),
            'fields': ('adm', 'created', )
        }),
    )

    actions = ['loadprice', 'cloneprice']

    def loadprice(self, request, queryset):
        umes = ""
        for pl in queryset:

            umes = umes + 'Process %s [NETLAN_price_%s_a.xls]: ' % (
                pl.name,
                pl.ef_date.strftime('%d.%m.%y')
            )
            wb = open_workbook(pl.file.path, formatting_info=False)
            try:
                s = wb.sheet_by_name('TDSheet')
                umes = 'Opened %s : %s, %s' % (s.name, s.nrows, s.ncols)
            except XLRDError:
                umes = umes + 'No Sheet'
                self.message_user(request, umes)
                return

            pl.productprice_set.all().delete()

            try:
                for row in range(1, s.nrows):

                    art = s.cell(row, 4).value

                    try:
                        product = Product.objects.get(art=art)

                        try:
                            EU = int(s.cell(row, 6).value)
                        except ValueError:
                            EU = 0

                        try:
                            RES0 = int(s.cell(row, 7).value)
                        except ValueError:
                            RES0 = 0

                        try:
                            RES1 = int(s.cell(row, 8).value)
                        except ValueError:
                            RES1 = 0

                        try:
                            RES2 = int(s.cell(row, 9).value)
                        except ValueError:
                            RES2 = 0

                        try:
                            PRED = int(s.cell(row, 11).value)
                        except ValueError:
                            PRED = 0

                        pl.productprice_set.create(
                            product=product,
                            price_eu=EU,
                            price_res0=RES0,
                            price_res1=RES1,
                            price_res2=RES2,
                            price_pred=PRED,
                        )
                    except Product.DoesNotExist:
                        umes = "%s, Error Find %s at Row %s" % (umes, art, row)

            except IndexError:
                umes = "%s, Error Array Row %s" % (umes, row)

            self.message_user(request, umes)

    loadprice.short_description = "Загрузка прайслиста"

    def cloneprice(self, request, queryset):
        from copy import deepcopy

        for my_obj in queryset:
            new_obj = deepcopy(my_obj)
            new_obj.id = None
            new_obj.name += '_clone'
            new_obj.save()
    cloneprice.short_description = "Клонирование прайслиста"


class ProductPriceAdmin(admin.ModelAdmin):
    list_display = (
        'product',
        'pricelist',
        'price_eu',
        'price_res0',
        'price_res1',
        'price_res2',
        'price_pred',
    )
    list_filter = ('pricelist', )
    search_fields = ('product', )


class PriceTypeAdmin(admin.ModelAdmin):
    list_display = ('ident', 'title', 'currency', 'customer')
    list_editable = ('customer', )


@transaction.atomic
def char_normalize_db(modeladmin, request, queryset):
    """ Normalize char values and remove duplicates """
    def find_duplicate(
        query,
        attr,
        exact=False,
        func=None,
        exist_list=[],
        non_exist_list=[]
    ):
        """ Find duplicates in query.
        attr: attribute on which duplicates are searched
        exact: flag for include exact matches in non_exist_list
        func: function allable function, The function is called the matches
        exist_list: list exist elements in query. Ref
        non_exist_list: list non exist elements in query. Ref
        """
        exist_list.append(query[:1][0])
        all = query[1:]
        for item in all:
            exist_flag = False
            for exist in exist_list:
                seq = difflib.SequenceMatcher(
                    a=getattr(exist, attr).lower(),
                    b=getattr(item, attr).lower()
                )
                if 0.9 < seq.ratio() < 1.0:
                    if (re.findall('[0-9]+', getattr(item, attr)) ==
                            re.findall('[0-9]+', getattr(exist, attr))):
                        exist_flag = True
                        if func:
                            func(item, exist)
                        non_exist_list.append(item)
                        break
                elif seq.ratio() == 1.0:
                    exist_flag = True
                    if exact:
                        non_exist_list.append(item)
                    break
            if not exist_flag:
                exist_list.append(item)

    query = ProductChar.objects.all().only('value')
    duplicate_values = []
    productchar_thread = threading.Thread(
        target=find_duplicate,
        kwargs={
            'query': query,
            'attr': 'value',
            'func': lambda item, exist: setattr(
                item, 'value', exist.value
            ),
            'non_exist_list': duplicate_values
        }
    )
    productchar_thread.start()
    query = Char.objects.all().only('name').prefetch_related('productchar_set')
    old_chars = []
    duplicate_chars = []
    char_thread = threading.Thread(
        target=find_duplicate,
        kwargs={
            'query': query,
            'attr': 'name',
            'exact': True,
            'exist_list': old_chars,
            'non_exist_list': duplicate_chars
        }
    )
    char_thread.start()
    productchar_thread.join()
    for i in duplicate_values:
        models.Model.save(i)
    char_thread.join()
    for i in duplicate_chars:
        old_char = (
            lambda name=i.name, chars=old_chars:
            [
                i for i in chars
                if difflib.SequenceMatcher(
                    a=i.name.lower(),
                    b=name.lower()
                ).ratio() > 0.9
            ]
        )()[0]
        for product_char in i.productchar_set.all():
            old_char.productchar_set.add(product_char)
        old_char.save()
    Char.objects.filter(productchar=None).delete()
    modeladmin.message_user(request, _('Done'))


char_normalize_db.short_description = _('Normalize chars in database')


@admin.register(Char)
class CharAdmin(GenericAbstractAdmin):
    list_display = ('name', 'position', 'ident', 'isactive')
    list_editable = ('position',)
    ordering = ('position',)
    actions = [char_normalize_db, ]

    fieldsets = (
        (None, {
            'fields': ('ident', 'name', 'position', 'isactive',)
        }),
    ) + GenericAbstractAdmin.fieldsets

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        # It is necessary to limit the nesting
        if db_field.name == "parent":
            kwargs["queryset"] = Char.objects.filter(
                parent__isnull=True
            ).order_by('position')
        return super(CharAdmin, self).formfield_for_foreignkey(
            db_field,
            request,
            **kwargs
        )


@admin.register(SectionMPTT)
class SectionMPTTAdmin(DraggableMPTTAdmin, GenericAbstractAdmin):
    list_display = (
        'tree_actions',
        'indented_title',
        'ident',
        'position',
        'parent',
        'ispublished',
    )
    list_display_links = ('indented_title'),

    fieldsets = (
        (None, {
            'fields': (
                'ispublished',
                'ident',
                'icon',
                'glyphicon',
                'position',
                'title',
                'brief',
                'parent'
            )
        }),
        (_('Params'), {
            'classes': ('collapse',),
            'fields': ('tags', 'keywords', 'desc',)
        }),
    ) + GenericAbstractAdmin.fieldsets

    exclude = ('products',)
    inlines = [ProductM2MInline, ContentFilesInline]
    actions = [rebuild_MPTT]
    summernote_fields = ('brief', )


@admin.register(ProductSections)
class ProductSectionsAdmin(DraggableMPTTAdmin, GenericAbstractAdmin):
    """
    Admin for intermediate table between Product and SectionMPTT
    """
    mptt_level_indent = 0
    list_display = (
        'tree_actions',
        'product'
    )
    list_display_links = ('product',)
    fieldsets = (
        (None, {'fields': ('product', 'sectionmptt')}),
    )
    list_filter = ('sectionmptt',)
    actions = [rebuild_MPTT]


admin.site.register(PriceType, PriceTypeAdmin)
admin.site.register(ProductPrice, ProductPriceAdmin)
admin.site.register(PriceList, PriceListAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Vendor, VendorAdmin)
admin.site.register(ProductRelatedType)


@admin.register(UnauthorizedBucket)
class UnauthorizedOrderBucket(admin.ModelAdmin):
    """All orders into bucket"""
    list_display = (
        'name',
        'company'
    )



