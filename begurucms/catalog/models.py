# coding=utf-8
from django.utils.translation import ugettext_lazy as _
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from begurucms.utils.models import (
    IdentActiveModel,
    AbstractModel,
)
from begurucms.trackemail.models import InternalMessageInterface


class Char(IdentActiveModel):
    name = models.CharField(_('Characteristic'), max_length=256)
    parent = models.ForeignKey(
        'self',
        verbose_name=_('Parent char'),
        related_name='childrens',
        null=True,
        blank=True,
        db_index=True
    )
    position = models.PositiveSmallIntegerField(
        verbose_name=_('Position'),
        null=True, blank=True)

    class Meta:
        verbose_name = _('Characteristic')
        verbose_name_plural = _('Characteristics')

    @property
    def has_top_group(self):
        return True if not self.parent and self.childrens.exists() else False

    @property
    def has_group(self):
        return True if self.childrens.exists() else False

    def __str__(self):
        return self.name


class ProductSections(MPTTModel):
    """
    intermediate table between Product and SectionMPTT
    """
    product = models.ForeignKey(Product, db_column='product_id')
    sectionmptt = models.ForeignKey(SectionMPTT, db_column='sectionmptt_id')
    parent = TreeForeignKey(
        'self',
        related_name='children',
        null=True,
        blank=True,
        db_index=True
    )

    def __str__(self):
        return "%s on %s" % (self.product, self.sectionmptt)

    class MPTTMeta:
        order_insertion_by = ['product']

    class Meta:
        db_table = 'catalog_product_sections'
        verbose_name = _('Product Section')
        verbose_name_plural = _('Product Sections')


class ProductCharAbstract(MPTTModel, AbstractModel):
    """Abstract carcase to Chars and Ordering table"""
    product = None
    char = models.ForeignKey(Char, verbose_name=_('Characteristic'))
    value = models.TextField(_('Value'), blank=True, null=True)
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='children'
    )

    class Meta:
        abstract = True


class OrderTableItem(ProductCharAbstract):
    product = models.ForeignKey(
        SubProduct,
        verbose_name=_('Product'),
        null=True,
        blank=True
    )

    def __str__(self):
        return self.char.name


class ProductChar(ProductCharAbstract):
    product = models.ForeignKey(
        Product,
        verbose_name=_('Product'),
        null=True,
        blank=True
    )
    char = models.ForeignKey(Char, verbose_name=_('Characteristic'))
    value = models.TextField(_('Value'), blank=True, null=True)
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='children'
    )

    class Meta:
        ordering = ['char__position']
        verbose_name = _('Product Characteristic')
        verbose_name_plural = _('Product Characteristics')

    class MPTTMeta:
        order_insertion_by = ['product', 'char']

    @property
    def has_group(self):
        return self.char.has_group

    def get_children(self):
        children_chars = self.char.childrens.filter(
            productchar__product__art=self.product.art
        ).distinct()
        return [
            pchar for char in children_chars
            for pchar in char.productchar_set.filter(
                product__art=self.product.art
            ).distinct().order_by('char__position')
            if pchar.value
        ]

    def __str__(self):
        return self.char.__str__()


class ProductItemAbstract(models.Model):
    """Abstract model to create Product and Bucket Items"""
    class Meta:
        abstract = True

    _product = models.ForeignKey(
        Product,
        verbose_name=_('Product'),
        null=True,
        blank=True
    )
    _sub_product = models.ForeignKey(
        SubProduct,
        verbose_name=_('Sub Product'),
        null=True,
        blank=True
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'product' in kwargs:
            self.product = kwargs.get('product')

    @property
    def product(self):
        """Choice is product or sub_product"""
        if self._sub_product:
            return self._sub_product
        elif self._product:
            return self._product
        return None

    @product.setter
    def product(self, obj):
        """Product setter choice is product or sub_product"""
        if obj is None:
            self._product = None
            self._sub_product = None
        elif obj._meta.model == Product:
            self._product = obj
            self._sub_product = None
        elif obj._meta.model == SubProduct:
            self._product = None
            self._sub_product = obj
        else:
            raise ValueError('Left operand not Product or SubProduct')

    def get_real_product(self):
        """get Product model from product or sub_product"""
        return (
            self._sub_product.product
            if self._sub_product else self._product
        )

    @property
    def sub_product(self):
        """Protect sub_product for error"""
        raise ValueError('sub_product protected. Use product.')

    @sub_product.setter
    def sub_product(self, obj):
        """Protect sub_product for error"""
        raise ValueError('sub_product protected. Use product.')

    @property
    def icon(self):
        """get icon url"""
        try:
            return self.get_real_product().content_files.filter(
                title='Icon'
            ).first().get_absolute_url()
        except AttributeError:
            return ""

    @property
    def annotation(self):
        """get product annotation"""
        if self._sub_product:
            return self._sub_product.product.annotation
        elif self._product:
            return self._product.annotation
        return None

    def save(self, *args, **kwargs):
        """Check  uniqueness among product and sub_product"""
        if all([self._product, self._sub_product]):
            raise ValueError('Use only one of Product and SubProduct')
        super().save(*args, **kwargs)

    def chars_dict(self):
        product_chars = self.get_real_product().productchar_set.all()
        chars = {i.char.name: i.value for i in product_chars}
        if self._sub_product:
            table_headers = [
                i.get('main')
                for i in self._sub_product.product.list_table().get('content')
            ]
            table_values = self._sub_product.list_table()
            order_table = dict()
            for header in table_headers:
                try:
                    order_table[header] = table_values[
                        table_headers.index(header)
                    ]
                except IndexError:
                    order_table[header] = ''
            chars.update(order_table)
        return chars

    def __str__(self):
        return '{product}'.format(product=self.product)


class ProductItem(ProductItemAbstract):
    """Products items"""
    pass


class BucketItem(ProductItemAbstract):
    """Products in bucket"""
    count = models.PositiveIntegerField(_('Count'), default=1)

    def __str__(self):
        return '{product} ({count})'.format(
            product=self.product,
            count=self.count
        )


class OrderBucketAbstract(InternalMessageInterface, models.Model):
    """Abstract model to create order from bucket"""
    updated = models.DateTimeField(verbose_name=_('Updated'), auto_now=True)
    bucket = models.ManyToManyField(
        BucketItem,
        verbose_name=_('Bucket'),
        related_name='order',
    )
    comment = models.TextField(verbose_name=_('Comment'), blank=True)

    class Meta:
        abstract = True


class UnauthorizedBucket(OrderBucketAbstract):
    """Bucket for unauthorized users"""
    project = models.CharField(
        verbose_name=_('Specification name'),
        max_length=128
    )
    name = models.CharField(verbose_name=_('Full name'), max_length=128)
    company = models.CharField(verbose_name=_('Company'), max_length=128)
    email = models.EmailField(verbose_name=_('Email'))
    city = models.CharField(verbose_name=_('City'), max_length=128)
    phone = models.CharField(
        verbose_name=_('Phone'),
        null=True,
        blank=True,
        max_length=64
    )

    def send_modeled_email(self, tpl_name=None, back=False, **kwargs):
        message = self.get_email_message(tpl_ident=tpl_name)
        if (not message):
            return
        if back:
            if message.bccemail:
                from_email = message.bccemail
            else:
                from_email = message.mdsettings.fromemail
            fromname = message.mdsettings.fromname
            to = self.email
            toname = '{0}'.format(self.name)
        else:
            from_email = self.email
            fromname = '{0}'.format(self.name)
            toname = ''
            if message.bccemail:
                to = message.bccemail
            else:
                to = message.mdsettings.fromemail
        self.send_full_email(
            from_email=from_email, to=to,
            from_name=fromname, to_name=toname,
            tpl_name=tpl_name,  **kwargs
        )
