{% extends "admin/base_site.html" %}
{% load admin_urls %}
{% load i18n %}

{% block breadcrumbs %}
<div class="breadcrumbs">
<a href="{% url 'admin:index' %}">{% trans 'Home' %}</a>
&rsaquo; <a href="{% url 'admin:app_list' app_label=model.app_label %}">{{ model.app_config.verbose_name }}</a>
&rsaquo; <a href="{% url model|admin_urlname:'changelist' %}">{{ model.verbose_name_plural|capfirst }}</a>
&rsaquo; {% trans 'Delete' %}
</div>
{% endblock %}


{% block content %}
    <h3>{% trans 'Choose new unit for this products: ' %}</h3>
    <ul>
        {% for obj in objects %}
            <li>{{ obj.art }}</li>
        {% endfor %}
    </ul>
    <form method="POST">
        {% csrf_token %}
        <div style="margin: 15px 0px">
            <label for="{{  form.unit.id_for_label }}">
                {{ form.unit.label }}
            </label>
            {{ form.unit }}
            <span style="display: none">
                {{ form.ids }}
            </span>
        </div>
        <input type="submit" value="{% trans "Yes, I'm sure" %}">
    </form>
{% endblock content %}
