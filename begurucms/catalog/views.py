# coding=utf-8
from pytils import translit
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic.edit import FormView
from django.views.generic.base import TemplateView
from django.http import JsonResponse, HttpResponse

from easy_pdf.views import PDFTemplateResponseMixin

from rest_framework.views import APIView
from rest_framework import generics, status
from rest_framework.views import Http404
from rest_framework.response import Response
from rest_framework.decorators import api_view

from begurucms.catalog.serializers import (
    BucketItemSerializer,
    BucketItemProductSerializer,
    SubProductToBucketSerializer,
    CompareSectionSerializer,
)
from begurucms.catalog.utils import BucketFacade
from begurucms.catalog.models import (
    SectionMPTT,
    Product,
    SubProduct,
    BucketItem,
    ProductItem,
)


class CatalogLeftMenu(TemplateView):
    template_name = 'begurucms/catalog_left-menu.tpl'

    def get(self, request, *args, **kwargs):
        """
        add goot nodes from SectionMPTT to get methods
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        def cache_recurce_tree(node, path=''):
            nonlocal sections
            node['id'] = path + ('/' if path else '') + node.get('id', '')
            for i in range(0, len(node.get('children', ''))):
                node['children'][i] = cache_recurce_tree(
                    sections[node['children'][i]], node['id']
                )
            if node['id']:
                node['id'] = reverse('catalog_section', args=[node['id']])
            return node

        # 'parent__id', 'rght', 'lft' needs for section.is_leaf_node()
        sections_qs = SectionMPTT.objects.filter(ispublished=True).only(
            'title',
            'ident',
            'parent__id',
            'rght',
            'lft'
        )
        sections = {}
        for section in sections_qs:
            if sections.get(section.parent_id):
                if sections[section.parent_id].get('children'):
                    sections[section.parent_id]['children'].append(section.id)
                else:
                    sections[section.parent_id]['children'] = [section.id, ]
            else:
                sections[section.parent_id] = {'children': [section.id, ]}
            if sections.get(section.id):
                sections[section.id]['text'] = section.title
                sections[section.id]['id'] = section.ident
            else:
                sections[section.id] = {
                    'text': section.title,
                    'id': section.ident
                }
        tree = cache_recurce_tree(node=sections[None],)['children']
        kwargs['tree'] = tree
        return super(CatalogLeftMenu, self).get(request, *args, **kwargs)


class CompareAbstractAPIView(APIView):
    """general data intoabstract class to compare pages"""
    # Use how: request.session[CompareAbstractAPIView.COMPARE_DATA]
    # format data: {'item_art': <ProductItem>, ...}
    COMPARE_DATA = 'compare_data'
    # Use how: request.session[CompareAbstractAPIView.COMPARE_SECTIONS]
    # format data: {'section_id': ['item_art', ...], ...}
    COMPARE_SECTIONS = 'compare_sections'

    class ProductNotInSection(Exception):
        pass

    def add_item(request, item):
        """add item and check on inclution in the section"""
        session_key = CompareAbstractAPIView.COMPARE_SECTIONS
        compare_session_key = CompareAbstractAPIView.COMPARE_DATA
        try:
            section_id = request.data[item.get_real_product().art]
        except KeyError:
            section_id = request.data['section']
        try:
            section = SectionMPTT.objects.get(
                id=int(section_id),
                products__in=[item.get_real_product()]
            )
        except SectionMPTT.DoesNotExist:
            raise CompareAbstractAPIView.ProductNotInSection
        if not request.session.get(session_key):
            request.session[session_key] = dict()
        if item.product.art in request.session[session_key].get(
            section.id, {}
        ):
            return
        try:
            request.session[session_key][section.id].append(item.product.art)
        except KeyError:
            request.session[session_key][section.id] = [item.product.art, ]
        if not request.session.get(compare_session_key):
            request.session[compare_session_key] = dict()
        request.session[compare_session_key][item.product.art] = item
        request.session.save()

    def rm_item(request, art):
        """rm item and section if item last in section"""
        try:
            request.session[CompareAbstractAPIView.COMPARE_DATA].pop(art)
        except KeyError:
            return
        session_key = CompareAbstractAPIView.COMPARE_SECTIONS
        sections = request.session.get(session_key)
        for id in sections.copy().keys():
            if art not in sections.get(id):
                continue
            sections[id].remove(art)
            if not sections[id]:
                sections.pop(id)
        request.session.save()

    def rm_section(request, id):
        """remove section and all product in section"""
        section = request.session.get(
            CompareAbstractAPIView.COMPARE_SECTIONS,
            dict()
        ).get(int(id))
        if not section:
            return
        for art in section:
            try:
                request.session[CompareAbstractAPIView.COMPARE_DATA].pop(art)
            except KeyError:
                pass
        try:
            session_key = CompareAbstractAPIView.COMPARE_SECTIONS
            request.session[session_key].pop(int(id))
        except KeyError:
            pass
        request.session.save()


class CompareSectionsListAPIView(
    CompareAbstractAPIView,
    generics.ListAPIView
):
    serializer_class = CompareSectionSerializer

    def get_queryset(self):
        try:
            id_list = (
                lambda sections=self.request.session[self.COMPARE_SECTIONS]:
                [i for i in sections.keys() if len(sections[i]) > 1]
            )()
            return SectionMPTT.objects.filter(id__in=id_list, ispublished=True)
        except KeyError:
            return SectionMPTT.objects.none()

    @api_view(['GET'])
    def section_data(request, id, format=None):
        """get section and chars data, including matrix
        Response:
            productscharacteristic: ['product_art', ...]
            characteristic: ['char_title', ''']
            matrix: [['char_value', 'char_value', ...], [...], ...]
        Indexes: matrix[product_id][char_id]
            """
        section = request.session.get(
            CompareSectionsListAPIView.COMPARE_SECTIONS,
            dict()
        ).get(int(id))
        if not section:
            raise Http404
        headers_response = list()
        items_response = list()
        matrix = list()
        products = request.session.get(
            CompareSectionsListAPIView.COMPARE_DATA,
            dict()
        )
        n = 0
        for item in products.values():
            if item.product.art not in section:
                continue
            items_response.append(item.product.art)
            chars = item.chars_dict()
            row = dict()
            for char in chars:
                if char not in headers_response:
                    headers_response.append(char)
                j = headers_response.index(char)
                n = max(j, n)
                row[j] = chars[char]
            matrix.append(row)
        # normalize matrix
        for row_index in range(0, len(matrix)):
            matrix[row_index] = (
                lambda matrix=matrix,
                n=n,
                i=row_index:
                [matrix[i].get(j, '') for j in range(0, n + 1)]
            )()
        return Response(
            {
                'products': items_response,
                'characteristic': headers_response,
                'matrix': matrix
            }
        )

    @api_view(['POST'])
    def delete_section(request, id, format=None):
        """remove section and all product in section"""
        section = request.session.get(
            CompareSectionsListAPIView.COMPARE_SECTIONS,
            dict()
        ).get(int(id))
        if not section:
            raise Http404
        CompareAbstractAPIView.rm_section(request, id)
        return Response({'status': 'updated'})


class CompareProductAPIView(CompareAbstractAPIView):
    """ViewSet to work with product to compare"""
    def get(self, request, format=None):
        data = [
            i for i in request.session.get(self.COMPARE_DATA, dict()).values()
        ]
        serializer = BucketItemProductSerializer(data, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """Add or update bucket items"""
        try:
            if not request.session.get(self.COMPARE_DATA):
                request.session[self.COMPARE_DATA] = dict()
            product = SubProduct.objects.get(art=request.data['product'])
            item = ProductItem(product=product)
            CompareAbstractAPIView.add_item(request, item)
            return Response({'status': 'updated'})
        except SubProduct.DoesNotExist:
            try:
                product = Product.objects.get(art=request.data['product'])
            except Product.DoesNotExist:
                raise Http404()
            sub_products = SubProduct.objects.filter(product=product)
            if not len(sub_products):
                item = ProductItem(product=product)
                try:
                    CompareAbstractAPIView.add_item(request, item)
                    return Response({'status': 'updated'})
                except CompareAbstractAPIView.ProductNotInSection:
                    return Response(
                        {'status': 'error', 'data': 'database error'}
                    )
            serializer = SubProductToBucketSerializer(sub_products, many=True)
            return Response({'status': 'data', 'data': serializer.data})
        except (KeyError, ValueError):
            response = Response('unvalid request.')
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response
        except CompareAbstractAPIView.ProductNotInSection:
            return Response({'status': 'error', 'data': 'database error'})

    @api_view(['POST'])
    def product_delete(request, format=None):
        """rm products from session"""
        if not request.session.get(CompareAbstractAPIView.COMPARE_DATA):
            request.session[CompareAbstractAPIView.COMPARE_DATA] = dict()
        data = (
            request.data
            if isinstance(request.data, list)
            else [request.data, ]
        )
        request_list = [i['product'] for i in data]
        try:
            for product in request_list:
                CompareAbstractAPIView.rm_item(request, product)
            return Response({'status': 'updated'})
        except CompareAbstractAPIView.ProductNotInSection:
            return Response({'status': 'error', 'data': 'database error'})

    @api_view(['POST'])
    def clear(request, format=None):
        """Clear all bucket"""
        request.session[CompareAbstractAPIView.COMPARE_DATA] = dict()
        request.session[CompareAbstractAPIView.COMPARE_SECTIONS] = dict()
        return JsonResponse({'status': 'clean'})

    @api_view(['POST'])
    def list(request, format=None):
        """Save list sub products"""
        try:
            request_list = [i for i in request.data]
            products = SubProduct.objects.filter(
                art__in=request_list
            )
            if len(request_list) != len(products):
                return Response({'status': 'error', 'data': 'database error'})
            for product in products:
                item = ProductItem(
                    product=product,
                )
                CompareAbstractAPIView.add_item(request, item)
            return Response({'status': 'updated'})
        except (KeyError, ValueError):
            response = Response('unvalid request.')
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response
        except CompareAbstractAPIView.ProductNotInSection:
            return Response({'status': 'error', 'data': 'database error'})


class BucketProductAPIView(APIView):
    """ViewSet to work with bucket, using bucket facade"""
    def get(self, request, format=None):
        """Get bucket items"""
        bucket = BucketFacade(request)
        serializer = BucketItemSerializer(bucket.data(), many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """Add or update bucket items"""
        try:
            bucket = BucketFacade(request)
            product = SubProduct.objects.get(art=request.data['product'])
            item = BucketItem(product=product, count=request.data['count'])
            bucket.update(item)
            return Response(
                {
                    'status': 'updated',
                    'data': BucketItemSerializer(
                        item
                    ).data
                }
            )
        except SubProduct.DoesNotExist:
            try:
                product = Product.objects.get(art=request.data['product'])
            except Product.DoesNotExist:
                raise Http404()
            sub_products = SubProduct.objects.filter(product=product)
            if not len(sub_products):
                item = BucketItem(product=product, count=request.data['count'])
                bucket.update(item)
                return Response(
                    {
                        'status': 'updated',
                        'data': BucketItemSerializer(
                            item
                        ).data
                    }
                )
            serializer = SubProductToBucketSerializer(sub_products, many=True)
            return Response({'status': 'data', 'data': serializer.data})
        except (KeyError, ValueError):
            response = Response('unvalid request.')
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response

    @api_view(['POST'])
    def list(request, format=None):
        """Save list sub products"""
        try:
            bucket = BucketFacade(request)
            request_dict = {i['product']: i['count'] for i in request.data}
            products = SubProduct.objects.filter(
                art__in=[i['product'] for i in request.data]
            )
            if len(request_dict) != len(products):
                response_data = dict()
                for product in products:
                    if product.art in request_dict:
                        response_data[product.art] = 'not found'
                return Response({'status': 'error', 'data': response_data})
            items_to_response = list()
            for product in products:
                item = BucketItem(
                    product=product,
                    count=request_dict[product.art]
                )
                bucket.update(item)
                items_to_response.append(item)
            return Response(
                {
                    'status': 'updated',
                    'data': BucketItemSerializer(
                        items_to_response,
                        many=True
                    ).data
                }
            )
        except (KeyError, ValueError):
            response = Response('unvalid request.')
            response.status_code = status.HTTP_400_BAD_REQUEST
            return response

    @api_view(['POST'])
    def clear(request, format=None):
        """Clear all bucket"""
        bucket = BucketFacade(request)
        bucket.clear()
        return JsonResponse({'status': 'clean'})


class BucketFormView(FormView):
    template_name = 'begurucms/bucket.tpl'
    form_class = None

    def form_valid(self, form):
        bucket = BucketFacade(self.request)
        try:
            bucket.send(form)
        except ValueError as e:
            return JsonResponse({'status': 'error', 'error': str(e)})
        return JsonResponse({'status': 'sended'})

    def form_invalid(self, form):
        return JsonResponse({'status': 'error', 'error': form._errors})

    def get_form_class(self):
        bucket = BucketFacade(self.request)
        return bucket.form_class()


def gen_specification_xl(request):
    bucket = BucketFacade(request)
    response = HttpResponse(
        content_type='application/vnd.ms-excel; charset=utf-8'
    )
    last_order = bucket.get_last_order_obj()
    first_name = translit.translify(
        last_order.project if last_order else ''
    ).lower()
    response['Content-Disposition'] = (
        'attachment; filename="{name}_specification.xls"'.format(
            name=first_name
        )
    )
    wb = bucket.get_specification_xl()
    wb.save(response)
    return response


class GenSpecificationPDFView(PDFTemplateResponseMixin, ListView):
    template_name = "begurucms/gen_specification_pdf.tpl"
    pdf_filename = 'specification.pdf'

    def get_queryset(self):
        bucket = BucketFacade(self.request)
        if bucket.get_last_order_obj() is not None:
            self.next_name = bucket.get_last_order_obj().project
        else:
            self.next_name = ''
        return bucket.get_last_order()

    def get_pdf_filename(self):
        return self.next_name + '_' + super().get_pdf_filename()


class CompareTemplateView(TemplateView):
    template_name = 'begurucms/compare.tpl'
