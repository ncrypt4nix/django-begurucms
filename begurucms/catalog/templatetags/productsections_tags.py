from django import template
from django.urls import reverse
from begurucms.catalog.models import (
    ProductSections,
    ProductChar,
    OrderTableItem
)
from begurucms.catalog.admin import (
    ProductSectionsAdmin,
    ProductCharAdmin,
    OrderTableItemAdmin
)

register = template.Library()


@register.filter
def class_name(value, arg=None):
    """
    check for belonging to classname or get classname
    :param value:
    :param arg:
    :return: classname or bool value if exist arg
    """
    if arg:
        return True if arg == value.__class__.__name__ else False
    return value.__class__.__name__


def admin_url(model=None, model_admin=None, id=None):
    """Get admin url in catalog app"""
    if not model or not model_admin or not id:
        return ''
    url = reverse('admin:{app}_{model}_changelist'.format(
        app='catalog',
        model=model._meta.model_name
    ))
    get_url = '?{filter}__id__exact={id}'.format(
        filter=model_admin.list_filter[0],
        id=id
    )
    return url + get_url


@register.simple_tag
def product_sections_url(id):
    """
    get url to product section admin
    :return: url to product section admin
    """
    return admin_url(
        model=ProductSections,
        model_admin=ProductSectionsAdmin,
        id=id
    )


@register.simple_tag
def product_chars_url(id):
    """
    get url to product chars admin
    :return: url to product chars admin
    """
    return admin_url(
        model=ProductChar,
        model_admin=ProductCharAdmin,
        id=id
    )


@register.simple_tag
def sub_product_chars_url(id):
    """
    get url to sub_product chars admin
    :return: url to sub_product chars admin
    """
    return admin_url(
        model=OrderTableItem,
        model_admin=OrderTableItemAdmin,
        id=id
    )
