import xlwt
from django.utils.translation import ugettext as _
from django.core.exceptions import ObjectDoesNotExist
from .forms import UnauthorizedBucketForm
from begurucms.catalog.models import UnauthorizedBucket


class BucketBackEndAbstract:
    """Abstract class to work with bucket"""
    def __init__(self, request):
        """Initialization backend"""
        raise AttributeError('Please define this method')

    def data(self):
        """Get list BucketItem from BackEnd"""
        raise AttributeError('Please define this method')

    def update(self, bucket_item):
        """Update BucketItem into bucket"""
        raise AttributeError('Please define this method')

    def clear(self):
        """Clear all data into bucket"""
        raise AttributeError('Please define this method')

    def form_class(self):
        """Get form class to backend"""
        raise AttributeError('Please define this method')

    def send(self, form):
        """Send method to backend"""
        raise AttributeError('Please define this method')

    def get_specification_xl(self):
        """get specification fron backend"""
        raise AttributeError('Please define this method')

    def get_last_order(self):
        """Get list bucketItem from last order"""
        raise AttributeError('Please define this method')


class UnauthorizedBucketBackEnd(BucketBackEndAbstract):
    """Work with bucket to unathurizer users"""
    session = None
    BUCKET = 'bucket'
    BUCKET_LAST_ORDER = 'order_id'

    def __init__(self, request):
        """Initialization backend"""
        self.session = request.session
        if not self.session.get(self.BUCKET):
            self.session[self.BUCKET] = dict()

    def data(self):
        """Get list BucketItem from BackEnd"""
        return [i for i in self.session[self.BUCKET].values()]

    def update(self, bucket_item):
        """Update BucketItem into bucket"""
        if int(bucket_item.count):
            self.session[self.BUCKET][bucket_item.product.art] = bucket_item
        else:
            self.session[self.BUCKET].pop(bucket_item.product.art, None)
        self.session.save()

    def clear(self):
        """Clear all data into bucket"""
        self.session[self.BUCKET] = dict()

    def form_class(self):
        """Get form class to backend"""
        return UnauthorizedBucketForm

    def send(self, form):
        """Send method to backend"""
        order = form.instance
        # to PostgreSQL
        # items = BucketItem.objects.bulk_create(self.data())
        # order.save()
        # order.bucket.set(items)
        items = self.data()
        if not items:
            raise ValueError('empty order')
        order.save()
        self.session[self.BUCKET_LAST_ORDER] = order.id
        for item in items:
            item.save()
            order.bucket.add(item)
        order.send_modeled_email(tpl_name='unauthorized_bucket_form')
        order.send_modeled_email(
            tpl_name='unauthorized_bucket_form_back',
            back=True
        )
        self.clear()

    def get_specification_xl(self):
        """get specification fron backend"""
        last_id = self.session.get(self.BUCKET_LAST_ORDER)
        last_order = UnauthorizedBucket.objects.get(id=last_id).bucket.all()
        wb = xlwt.Workbook()
        ws = wb.add_sheet('Scpecification')
        headers = [
            _('Art'),
            _('Count'),
            _('Unit')
        ]
        for header in headers:
            ws.write(0, headers.index(header), header)
        i = 1
        for item in last_order:
            ws.write(i, 0, item.product.art)
            ws.write(i, 1, item.count)
            ws.write(i, 2, 'шт')
            i += 1
        return wb

    def get_last_order(self):
        """Get list bucketItem from last order"""
        last_id = self.session.get(self.BUCKET_LAST_ORDER)
        last_order = UnauthorizedBucket.objects.get(id=last_id).bucket.all()
        return last_order

    def get_last_order_obj(self):
        """Get last order id"""
        last_id = self.session.get(self.BUCKET_LAST_ORDER)
        try:
            last_order = UnauthorizedBucket.objects.get(id=last_id)
        except ObjectDoesNotExist:
            last_order = None
        return last_order


class BucketFacade:
    """Facade for work with catalog bucket"""
    def __init__(self, request):
        """Initialization facade object from request"""
        self.backend = UnauthorizedBucketBackEnd(request)

    def form_class(self):
        """Get needed form"""
        return self.backend.form_class()

    def send(self, form):
        """Send facade data to"""
        self.backend.send(form)

    def clear(self):
        """clear facade data"""
        self.backend.clear()

    def update(self, bucket_item):
        """update bucket item"""
        self.backend.update(bucket_item)

    def data(self):
        """Get list BucketItem from facade"""
        return self.backend.data()

    def get_specification_xl(self):
        """get generated Workbook not saved file from backend"""
        return self.backend.get_specification_xl()

    def get_last_order(self):
        """Get list bucketItem from last order"""
        return self.backend.get_last_order()

    def get_last_order_obj(self):
        """Get list bucketItem from last order"""
        return self.backend.get_last_order_obj()
