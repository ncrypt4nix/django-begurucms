from django import forms
from django.core.urlresolvers import reverse
from django.http import Http404
from django.views.generic.edit import FormView
from django.utils.translation import ugettext_lazy as _
from .models import Product


class ChangeUnitAdminForm(forms.Form):
    """Form to update unit on Product"""
    unit = forms.CharField(label=_("Unit: "))
    ids = forms.ModelMultipleChoiceField(
        queryset=Product.objects.all(),
    )


class ChangeUnitAdminFormView(FormView):
    """Choose unit and change on queryset"""
    template_name = 'admin/catalog/change_unit_admin.tpl'
    form_class = ChangeUnitAdminForm
    permission_required = 'catalog.product'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.success_url = reverse(
            'admin:{}_{}_changelist'.format(
                'catalog',
                Product._meta.model_name
            )
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ids = self.request.GET.get('ids')
        if not ids:
            raise Http404()
        id_list = ids.split(',')
        context['form'].initial.update({'ids': id_list})
        context['objects'] = Product.objects.filter(
            id__in=id_list
        ).values('art')
        context['model'] = Product._meta
        return context

    def form_valid(self, form):
        form.cleaned_data['ids'].update(unit=form.cleaned_data['unit'])
        return super().form_valid(form)
