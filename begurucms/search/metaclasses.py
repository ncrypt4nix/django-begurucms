from haystack import indexes


class SearchBaseMetaclass(indexes.DeclarativeMetaclass):
    """metaclass for search class factory"""
    def __new__(cls, name, bases, dct):
        bases += (indexes.SearchIndex, indexes.Indexable)
        # init attrs for template
        dct['get_model'] = lambda self: dct['model']
        published = dct.get('published')
        dct['published'] = 'ispublished' if not published else published
        dct['published'] = indexes.BooleanField(model_attr=dct['published'])
        dct['text'] = indexes.CharField(document=True, use_template=True)
        dct['rendered'] = indexes.CharField(use_template=True, indexed=False)
        dct['filters'] = indexes.FacetMultiValueField()
        if not dct.get('prepare_filters'):
            dct['prepare_filters'] = lambda self, obj: []
        dct['autocomplete_data'] = indexes.EdgeNgramField(
            model_attr=dct['autocomplete_data']
        )
        if isinstance(dct['faceted_type'], str):
            dct['faceted_type'] = indexes.CharField(
                model_attr=dct['faceted_type'],
                faceted=True
            )
        return super(SearchBaseMetaclass, cls).__new__(cls, name, bases, dct)
