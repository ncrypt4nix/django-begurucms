from django.core.cache import cache
from rest_framework.decorators import api_view
from rest_framework.response import Response
from haystack.generic_views import FacetedSearchView
from begurucms.optins.models import Optin
from begurucms.search.forms import SearchFormCustomized
from begurucms.search.search_indexes import ProductIndex
from begurucms.search.models import SearchBoosting
from begurucms.search.querysets import SearchQuerySetCustomized


class SearchViewCustomized(FacetedSearchView):
    """Customized search view"""
    queryset = SearchQuerySetCustomized()
    form_class = SearchFormCustomized
    facet_fields = ['faceted_type', 'filters']

    def get_queryset(self):
        return SearchViewCustomized.generate_queryset(
            self.request,
            super().get_queryset()
        )

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if (context['form'].model == ProductIndex.model):
            context['rendered_square'] = True
        context['result_count'] = self.queryset.count()
        filters = context.get('facets', {}).get('fields', {}).get('filters')
        if not filters:
            return context
        new_format = {}
        for flt in filters:
            foo = flt[0].split('___')
            title, value = foo[0], ''.join(foo[1:])
            try:
                new_format[title].append((value, flt[1]))
            except KeyError:
                new_format[title] = [(value, flt[1])]
        context['filters'] = new_format
        return context

    def generate_queryset(request, queryset=SearchQuerySetCustomized()):
        if request.user.is_superuser:
            return queryset
        max_lvl = 0
        try:
            oid = request.session.get('optin_id')
            optin = Optin.objects.get(pk=oid)
            max_lvl = 50 if optin.employee else 25 if getattr(optin, 'partner', None) else 0
        except Optin.DoesNotExist:
            pass
        queryset = queryset.exclude(access_level__gt=max_lvl)
        user_staff = request.user.is_staff
        search_boost = cache.get('search_boost')
        if not search_boost:
            search_boost = [
                (i.phrase, i.boost)
                for i in SearchBoosting.objects.all()
            ]
            cache.set('search_boost', search_boost, 5 * 60)
        for key, value in search_boost:
            queryset = queryset.boost(key, value)
        return queryset if user_staff else queryset.filter(published=True)

    @api_view(['GET'])
    def autocomplete(request):
        query = SearchViewCustomized.generate_queryset(request).autocomplete(
            autocomplete_data=request.GET.get('q', ' ')
        )[:10]
        return Response(
            [
                {'name': i.autocomplete_data}
                for i in query
            ]
        )
