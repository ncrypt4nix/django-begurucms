from django.contrib import admin
from begurucms.search.models import SearchBoosting, SearchHistory


@admin.register(SearchBoosting)
class SearchBoostingAdmin(admin.ModelAdmin):
    list_display = ('phrase', 'boost')


@admin.register(SearchHistory)
class SearchHistoryAdmin(admin.ModelAdmin):
    list_display = ('query', 'model_type', 'count', 'updated')
    readonly_fields = list_display
