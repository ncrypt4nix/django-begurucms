from haystack import indexes
from begurucms.catalog.models import Product
from begurucms.pages.models import Publication
from begurucms.useful_resources.models import Resource
from .metaclasses import SearchBaseMetaclass


class ProductIndex(metaclass=SearchBaseMetaclass):
    """Index to Product model"""
    model = Product
    autocomplete_data = 'art'
    faceted_type = indexes.FacetMultiValueField()
    rendered_square = indexes.CharField(use_template=True, indexed=False)
    art = indexes.CharField(model_attr='art')

    def prepare_faceted_type(self, obj):
        """Prepare sections to faceted"""
        return [i for i in set([i.title for i in obj.sections.all()])]

    def prepare_filters(self, obj):
        """Prepare filters faceted to products"""
        return [
            '{}___{}'.format(i.filter.title, i.value)
            for i in obj.suitable_filters.all()
            if i.filter.ispublished
        ]


class PublicationIndex(metaclass=SearchBaseMetaclass):
    """Index to Product model"""
    model = Publication
    autocomplete_data = 'title'
    faceted_type = 'ptype'
    access_level = indexes.IntegerField(model_attr='accesslevel')

    def prepare_access_level(self, obj):
        return max(obj.accesslevel, obj.ptype.accesslevel)

    def index_queryset(self, using=None):
        qs = super().index_queryset(using)
        return qs.exclude(ptype__ident='product_group_inline')


class ResourceIndex(metaclass=SearchBaseMetaclass):
    """Index to Product model"""
    model = Resource
    autocomplete_data = 'title'
    faceted_type = 'resource_type'
    access_level = indexes.IntegerField(model_attr='accesslevel')
