from django import forms
from haystack.forms import FacetedModelSearchForm, model_choices
from haystack.utils import get_model_ct
from haystack.utils.app_loading import haystack_get_model
from django.contrib.contenttypes.models import ContentType
from begurucms.search.search_indexes import ProductIndex
from begurucms.search.models import SearchHistory


class SearchFormCustomized(FacetedModelSearchForm):
    """Custom form using specific algorithms to firm"""
    product_set = (
        get_model_ct(ProductIndex.model),
        ProductIndex.model._meta.verbose_name_plural
    )

    def __init__(self, *args, **kwargs):
        self.model = None
        self.selected_facets = kwargs.pop("selected_facets", [])
        super().__init__(*args, **kwargs)
        self.fields['models'] = forms.ChoiceField(
            widget=forms.RadioSelect,
            choices=model_choices(),
            required=False
        )

    def get_models(self):
        """add type list to models"""
        if (self.cleaned_data['models'] and not
                isinstance(self.cleaned_data['models'], list)):
            self.cleaned_data['models'] = [self.cleaned_data['models']]
        models = super().get_models()
        if not models:
            self.model = (
                ProductIndex.model
                if self.product_set in model_choices()
                # Not sure about zeroes into indexes
                else haystack_get_model(*model_choices()[0][0].split('.'))
            )
        else:
            self.model = models[0]
        return [self.model]

    def search(self):
        selected_facets = self.cleaned_data.get(
            'selected_facets',
            ''
        ).replace(' ', '\\ ')
        if selected_facets:
            self.cleaned_data['selected_facets'] = selected_facets
        sqs = super().search()
        q = self.cleaned_data.get("q", None)
        if q:
            query_max_len = SearchHistory._meta.get_field('query').max_length
            try:
                SearchHistory.objects.update_or_create(
                    query=q if len(q) <= query_max_len else q[:query_max_len],
                    model_type=ContentType.objects.get_for_model(self.model)
                )
            except ValueError:
                return sqs.none()
        art_exact = sqs.filter(art__exact=q)
        return art_exact if art_exact.count() == 1 else sqs
