from django.conf.urls import url
from .views import SearchViewCustomized


urlpatterns = [
    url(r'^search/$', SearchViewCustomized.as_view(), name='haystack_search'),
    url(
        r'search/autocomplete/$',
        SearchViewCustomized.autocomplete,
        name='haystack_search-autocomplete'
    )
]
