from django.utils import timezone
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext as _


class SearchBoosting(models.Model):
    """uses to boosted in search queryset"""
    phrase = models.CharField(_('Phrase'), max_length=80, unique=True)
    boost = models.FloatField(_('Gain'))

    class Meta:
        verbose_name = _('Search Boosting')
        verbose_name_plural = verbose_name


class SearchHistory(models.Model):
    """History all search queries without repeat"""
    query = models.CharField(_('User Query'), max_length=128)
    model_type = models.ForeignKey(
        ContentType,
        on_delete=models.CASCADE,
        verbose_name=_("Query Type")
    )
    count = models.PositiveIntegerField(_('Count'), default=1)
    updated = models.DateTimeField(
        _('Last Query'),
        default=timezone.now
    )

    class Meta:
        unique_together = ('query', 'model_type')
        verbose_name = _('Search History')
        verbose_name_plural = verbose_name

    def save(self, *args, **kwargs):
        if self.pk:
            self.count += 1
        super().save(*args, **kwargs)
