from __future__ import absolute_import, unicode_literals
from django.core import management
from django.conf import settings
from celery.decorators import periodic_task
from datetime import timedelta


if not settings.DEBUG:
    @periodic_task(run_every=timedelta(days=1))
    def rebuild_solr_index():
        management.call_command('rebuild_index', '--noinput')
