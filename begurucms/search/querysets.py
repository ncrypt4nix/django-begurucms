from haystack.backends import SQ
from haystack.query import SearchQuerySet


class SearchQuerySetCustomized(SearchQuerySet):
    def auto_query(self, query_string, fieldname="content"):
        # adding SQ fixes a strange case-insensitive bug
        sqs = self.filter(
            SQ(**{fieldname + '__icontains': query_string}) |
            SQ(**{fieldname + '__contains': query_string})
        )
        return (
            sqs
            if sqs
            else self.filter(
                **{fieldname + '__fuzzy': query_string}
            )
        )

    def facet_counts(self):
        facet = super().facet_counts()
        for field in facet.get('fields', []):
            facet['fields'][field] = [
                i
                for i in facet['fields'][field]
                if i[1]
            ]
        return facet
