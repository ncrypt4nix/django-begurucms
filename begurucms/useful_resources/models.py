# coding=utf-8
import os
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.db import models
from django.contrib.auth.models import Permission
from django.db.models import Q
from mptt.models import MPTTModel, TreeForeignKey
from begurucms.utils.models import (
    IdentAbstractModel,
    PublishedModel,
)
from begurucms.pages.models import CFileStorage


class ResourceType(MPTTModel, IdentAbstractModel):
    title = models.CharField(_('Title'), max_length=250)
    parent = TreeForeignKey(
        'self',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='children'
    )
    show_preview = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Type of Useful Resource')
        verbose_name_plural = _('Types of Useful Resources')
        ordering = ['pos']

    def __str__(self):
        return self.title


class ResourceManager(models.Manager):
    use_for_related_fields = True

    def get_queryset(self):
        return super(ResourceManager, self).get_queryset()

    def published(self, *args, **kwargs):
        alevel = kwargs['alevel'] if 'alevel' in kwargs else 0
        return self.get_queryset().filter(
            accesslevel__lte=alevel,
            ispublished=True
        )

    def filter_to_user(self, user=None):
        if not user or user.is_anonymous:
            return self.published()
        alevel = 0
        A_LVL_STR = 'accesslevel_'
        for i in Permission.objects.filter(Q(group__user=user) | Q(user=user)):
            if A_LVL_STR in i.codename:
                try:
                    tmp = int(i.codename.replace(A_LVL_STR, ''))
                    alevel = tmp if alevel < tmp else alevel
                except ValueError:
                    pass
        return self.published(alevel=alevel)

    def model(self, *args, **kwargs):
        return self.model


class Resource(PublishedModel):
    resource_type = models.ForeignKey(
        ResourceType,
        verbose_name=_('Resouce Type')
    )
    vendors = models.ManyToManyField(
        'catalog.Vendor',
        verbose_name=_('Vendor'),
        blank=True
    )
    products = models.ManyToManyField(
        'catalog.Product',
        verbose_name=_('Product'),
        blank=True
    )
    resource_file = models.FileField(
        _('Resource File'),
        storage=CFileStorage(),
        max_length=250,
        upload_to=upload_to_resource,
        blank=True
    )
    resource_text = models.CharField(
        _('Text Resource'),
        max_length=300,
        help_text=_('Might be external link'),
        blank=True
    )
    thumb_image = models.ImageField(
        _('Preview'),
        upload_to=upload_to_resource,
        storage=CFileStorage(),
        blank=True,
        max_length=250,
        help_text=_('Can be made automaticaly or set manualy')
    )
    printed = models.BooleanField(
        _('Is printed exist'),
        default=False,
        help_text=_('Is there are printed version?')
    )
    accesslevel = models.IntegerField(
        _('Access level'),
        blank=True,
        null=True,
        default=0
    )
    objects = ResourceManager()

    class Meta:
        verbose_name = _('Useful Resource')
        verbose_name_plural = _('Useful Resources')
        ordering = ['-created', 'published']
        permissions = (
            (
                'accesslevel_1',
                'Access Level 0-1. Can see resource with access level 0-1'
            ),
            (
                'accesslevel_25',
                'Access Level 0-25. Can see resource with access level 0-25'
            ),
            (
                'accesslevel_50',
                'Access Level 0-50. Can see resource with access level 0-50'
            ),
            (
                'accesslevel_75',
                'Access Level 0-75. Can see resource with access level 0-75'
            ),
            (
                'accesslevel_100',
                'Access Level 0-100. Can see resource with access level 0-100'
            ),
        )

    def check_file_exist(self):
        if self.resource_file:
            url = os.path.join(
                settings.EXTERNAL_MEDIA_ROOT,
                self.resource_file.name
            )
            if os.path.isfile(url):
                return True
        return False

    def get_absolute_url(self):
        url = "{0}".format(reverse('resource_fileview', args=[str(self.id)]))
        if self.check_file_exist():
            return url
        if self.resource_text:
            return self.resource_text
        return "{0}{1}".format(settings.SERVPREFIX, url)

    def get_absolute_url_thumb(self):
        if self.thumb_image:
            return "{0}?view=thumb".format(self.get_absolute_url())
        return ''

    def __str__(self):
        return self.title
