from rest_framework import serializers
from .models import Resource


class ResourceSerializer(serializers.ModelSerializer):
    """Resources Serializer to json"""
    absolute_url = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    vendors = serializers.SerializerMethodField()
    preview = serializers.SerializerMethodField()
    updated = serializers.SerializerMethodField()

    class Meta:
        # Warning! change queryset view if change this
        model = Resource
        fields = (
            'title',
            'absolute_url',
            'type',
            'vendors',
            'preview',
            'updated'
        )
        read_only_fields = fields

    def get_updated(self, obj):
        return obj.updated.date()

    def get_absolute_url(self, obj):
        return (
            obj.get_absolute_url()
            if obj.check_file_exist() or obj.resource_text
            else ''
        )

    def get_type(self, obj):
        return obj.resource_type.title

    def get_vendors(self, obj):
        return [i.title for i in obj.vendors.all()]

    def get_preview(self, obj):
        return obj.get_absolute_url_thumb()
