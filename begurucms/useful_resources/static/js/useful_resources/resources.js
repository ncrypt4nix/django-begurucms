if (!resourcesUrl)
    console.log('resourcesUrl not init')

if (!resourceFiltersUrl)
    console.log('resourceFiltersUrl not init')

if (!allValue)
    console.log('allValue not init')

$(function() {
    var download_app = new Vue({
        el: '#download-app-id',
        data: {
            certificates: [],
            page: 1,
            max_page: 1,
            by_vendor: allValue,
            by_type: allValue,
            by_sub_type: allValue,
            by_section: allValue,
            by_model: allValue,
            vendors: [],
            types: [],
            sub_types: [],
            sections: [],
            models: [],
            preview_flag:  false,
            json_filter:  [
                {
                    name: 'vendor', 
                    value: allValue
                },
                {
                    name: 'type',
                    value: allValue
                },
                {
                    name: 'sub_type',
                    value: allValue
                },
                {
                    name: 'section',
                    value: allValue
                },
                {
                    name: 'model',
                    value: allValue
                },
            ],
        },
        methods: {
            getCertificates: function(page=1) {
                this.page = page
                this.certificates = []
                url = this.urlGetParams(resourcesUrl, {'page': page})
                this.$http.get(url).then(response => {
                    this.max_page = response.body.num_pages
                    this.certificates = response.body.results
                })
            },
            getNavigatePage: function(page) {
                this.getCertificates(page)
            },
            getFilters: function() {
                url = this.urlGetParams(resourceFiltersUrl)
                this.$http.get(url).then(response => { 
                    this.preview_flag = response.body.preview_flag
                    this.vendors = response.body.vendors
                    this.types = response.body.resource_types
                    this.sub_types = response.body.resource_sub_types
                    this.sections = response.body.sections
                    this.models = response.body.products
                })
            },
            urlGetParams: function(fulldomain, params=null) {
                url = fulldomain
                first_param = true
                for (next of this.json_filter) {
                    if (next.value != allValue) {
                        url += first_param ? '?' : '&'
                        first_param = false
                        url += next.name + '=' + next.value
                    }
                }
                for (var key in params){
                    url += first_param ? '?' : '&'
                    first_param = false
                    url += key + '=' + params[key]
                }
                return url
            }
        },
        watch: {
            by_vendor: function () {
                this.json_filter.filter(item => item.name == 'vendor')[0].value = this.by_vendor
                this.getFilters()
                this.getCertificates()
            },
            by_type: function () {
                this.json_filter.filter(item => item.name == 'type')[0].value = this.by_type
                if (this.by_sub_type == allValue) {
                    if (this.by_section == allValue) {
                        if (this.by_model == allValue) {
                            this.getFilters()
                            this.getCertificates()
                        }
                        else {
                            this.by_model = allValue
                        }
                    }
                    else {
                        this.by_section = allValue
                    }
                }
                else {
                    this.by_sub_type = allValue
                }
            },
            by_sub_type: function () {
                this.json_filter.filter(item => item.name == 'sub_type')[0].value = this.by_sub_type
                if (this.by_section == allValue) {
                    if (this.by_model == allValue) {
                        this.getFilters()
                        this.getCertificates()
                    }
                    else {
                        this.by_model = allValue
                    }
                }
                else {
                    this.by_section = allValue
                }
            },
            by_section: function () {
                this.json_filter.filter(item => item.name == 'section')[0].value = this.by_section
                if (this.by_model == allValue) {
                    this.getFilters()
                    this.getCertificates()
                }
                else {
                    this.by_model = allValue
                }
            },
            by_model: function () {
                this.json_filter.filter(item => item.name == 'model')[0].value = this.by_model
                this.getFilters()
                this.getCertificates()
            },
        },
        components: {
            'certificate-pagination': {
                props: {
                    page: Number,
                    max_page: Number
                },
                methods: {
                    navClicked: function(event) {
                        this.$emit('nav-click', Number(event.target.text))
                    }
                },
                template: `
                    <nav aria-label="certificate navigation">
                        <ul class="pagination">
                            <li v-for="num in max_page" 
                                :class="[num == page ? 'active' : '', 'page-item']"
                                v-on:click="navClicked"
                            >
                                <a class="page-link" href="#">
                                    {{ num }}
                                </a>
                            </li>
                        </ul>
                    </nav>
                `,
            }
        },
        delimiters: ['${', '}'],
        created: function() {
            for (param of requestParams) {
                for (next of this.json_filter) {
                    if (param.name == next.name) {
                        next.value = param.value
                        key = 'by_' + next.name 
                        this[key] = param.value
                    }
                }
            }
            this.getFilters()
            this.getCertificates()
        }
    })
})

