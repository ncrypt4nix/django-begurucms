from django.conf.urls import include, url
from begurucms.useful_resources import views

urlpatterns = [
    url(r'^resources/$', views.ResourceView.as_view(), name="resource_view"),
    url(
        r'^api/resources/',
        include((views.router.urls, 'resources'), namespace='resources-api')
    ),
]
