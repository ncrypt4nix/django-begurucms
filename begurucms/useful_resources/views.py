from django.db.models import Q
from django.views.generic import TemplateView
from django.utils.translation import ugettext_lazy as _
from rest_framework import viewsets, routers
from rest_framework.response import Response
from rest_framework.decorators import action
from begurucms.catalog.models import Product, Vendor, SectionMPTT
from begurucms.core.pagination import ResultAndMaxPagePaginator
from begurucms.useful_resources.models import Resource, ResourceType
from .serializers import ResourceSerializer


class ResourceView(TemplateView):
    template_name = 'useful_resources/resource_view.tpl'
    all_value = _('All')
    optin_requered = False
    queryset = None

    def get_context_data(self, **kwargs):
        context = super(ResourceView, self).get_context_data(**kwargs)
        context['all_value'] = self.all_value
        return context


class ResourceAPIView(viewsets.ReadOnlyModelViewSet):
    """Getter json resource view"""
    serializer_class = ResourceSerializer
    pagination_class = ResultAndMaxPagePaginator
    filter_kwargs = {
        'vendor': 'vendors__title',
        'section': 'products__sections__title',
        'model': 'products__art',
    }
    filter_type_kwargs = {
        'type': 'title',
        'sub_type': 'title',
    }

    def get_resources_queryset(self, user=None):
        """logics for permisions to resources"""
        return Resource.objects.filter_to_user(user=user)

    def get_queryset(self):
        """Get optimize filtered resources"""
        queryset = self.get_resources_queryset(
            self.request.user
        ).prefetch_related(
            'vendors',
            'resource_type'
        ).only(
            'vendors__title',
            'resource_type__title',
            'title',
            'updated',
            'resource_text'
        )
        qs_kwars = (
            lambda GET=self.request.GET, kwargs=self.filter_kwargs:
            {kwargs[arg]: GET[arg] for arg in kwargs if arg in GET}
        )()
        qs_type_kwargs = (
            lambda GET=self.request.GET, kwargs=self.filter_type_kwargs:
            {kwargs[arg]: GET[arg] for arg in kwargs if arg in GET}
        )()
        resource_types = ResourceType.objects.filter(
            **qs_type_kwargs
        ).get_descendants(include_self=True)
        queryset = queryset.filter(resource_type__in=resource_types)
        return queryset.filter(**qs_kwars).distinct()

    @action(detail=False)
    def get_filter_items(self, request):
        """Get filtered items to filter"""
        preview_flag = False
        vendors = Vendor.objects.published()
        resources = self.get_resources_queryset(request.user)
        resource_types = ResourceType.objects.filter(
            Q(level=0) &
            (
                Q(resource__in=resources) |
                Q(
                    children__in=ResourceType.objects.filter(
                        resource__in=resources
                    )
                )
            )
        ).distinct()
        resource_sub_types = ResourceType.objects.none()
        sections = SectionMPTT.objects.filter(
            ispublished=True
        )
        products = Product.objects.published().filter(
            ispublished=True
        )
        if 'type' in request.GET:
            resource_types_filter = ResourceType.objects.filter(
                title=request.GET['type']
            ).get_descendants(include_self=True)
            resources = resources.filter(
                resource_type__in=resource_types_filter
            )
            resource_sub_types = ResourceType.objects.filter(
                title=request.GET['type']
            ).get_descendants().filter(resource__in=resources)
            if 'sub_type' in request.GET:
                resource_types_filter = ResourceType.objects.filter(
                    title=request.GET['sub_type']
                ).get_descendants(include_self=True)
                resources = resources.filter(
                    resource_type__in=resource_types_filter
                )
            if resource_types_filter.count() == 1:
                preview_flag = resource_types_filter[0].show_preview
        products = products.filter(resource__in=resources)
        sections = sections.filter(products__in=products)
        if 'section' in request.GET:
            products = products.filter(
                sections__title=request.GET['section']
            )
        sections_values = []
        for i in sections.distinct().values('title'):
            if i not in sections_values:
                sections_values.append(i)
        return Response({
            'vendors': vendors.values('title'),
            'resource_types': resource_types.values('title'),
            'resource_sub_types': resource_sub_types.distinct().values(
                'title'
            ),
            'sections': sections_values,
            'products': products.distinct().values('art'),
            'preview_flag': preview_flag
        })


router = routers.SimpleRouter()
router.register(r'get-resources', ResourceAPIView, 'resources')
