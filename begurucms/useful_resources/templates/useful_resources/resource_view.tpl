{% extends "base.tpl" %}
{% load static %}
{% load django_bootstrap_breadcrumbs %}
{% load bgcms_tags %}
{% load i18n %}

{% block breadcrumbs %}
	{{ block.super }}
	{% breadcrumb "Downloads" "resource_view" %}
{% endblock breadcrumbs %}

{% block headers %}
    <script>
        // init Vue.js varriables
        var resourcesUrl = '{% url 'resources-api:resources-list' %}'
        var allValue = '{{ all_value }}'
        var resourceFiltersUrl = '{% url 'resources-api:resources-get-filter-items' %}'
        var requestParams = [
            {% for key, value in request.GET.items %}
                {
                    name: '{{ key }}',
                    value: '{{ value }}'
                },
            {% endfor %}
        ]
    </script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource"></script>
    <script src="{% static "js/useful_resources/resources.js" %}"></script>
    <link rel="stylesheet" href="{% static 'css/useful_resources/useful_resources.css' %}">
{% endblock %}

{% block fulltitle %}
    {% trans 'Downloads' %}
{% endblock fulltitle %}

{% block pagecontent %}
    <div id="download-app-id" class="container resources">
        <div class="row">
            <form class="form-row row">
                <div v-if="vendors.length > 1" class="col-md form-group">
                    <label for="vendors-select">
                        {% trans 'By vendors' %}
                    </label>
                    <select id="vendors-select" v-model="by_vendor" class="form-control" required>
                        <option value={{ all_value }}>{{ all_value }}</option>
                        <option v-for="vendor in vendors" :value="vendor.title">${ vendor.title }</option>
                    </select>
                </div>
                <div v-if="types.length" class="col-md form-group">
                    <label for="resource_types-select">
                        {% trans 'By type' %}
                    </label>
                    <select id="resource_types-select" v-model="by_type" class="form-control" required>
                        <option value={{ all_value }}>{{ all_value }}</option>
                        <option v-for="type in types" :value="type.title">${ type.title }</option>
                    </select>
                </div>
                <div v-if="sub_types.length" class="col-md form-group">
                    <label for="resource_sub_types-select">
                        {% trans 'By subtype' %}
                    </label>
                    <select id="resource_sub_types-select" v-model="by_sub_type" class="form-control" required>
                        <option value={{ all_value }}>{{ all_value }}</option>
                        <option v-for="sub_type in sub_types" :value="sub_type.title">${ sub_type.title }</option>
                    </select>
                </div>
                <div v-if="sections.length" class="col-md form-group">
                    <label for="sections-select">
                        {% trans 'By section' %}
                    </label>
                    <select id="section-select" v-model="by_section" class="form-control" required>
                        <option value={{ all_value }}>{{ all_value }}</option>
                        <option v-for="section in sections" :value="section.title">${ section.title }</option>
                    </select>
                </div>
                <div v-if="models.length" class="col-md form-group">
                    <label for="products-select">
                        {% trans 'By model' %}
                    </label>
                    <select id="model-select" v-model="by_model" class="form-control" required>
                        <option value={{ all_value }}>{{ all_value }}</option>
                        <option v-for="model in models" :value="model.art">${ model.art }</option>
                    </select>
                </div>
            </form>
        </div>
        <div v-if="!preview_flag" class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="coll">{% trans 'Title' %}</th>
                        <th scope="coll">{% trans 'Type' %}</th>
                        <th v-if="vendors.length > 1" scope="coll">{% trans 'For products' %}</th>
                        <th scope="coll">{% trans 'Action' %}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="certificate in certificates">
                        <td>${ certificate.title }</td>
                        <td>${ certificate.type }</td>
                        <td v-if="vendors.length > 1">
                            <ul class="list-unstyled">
                                <li v-for="vendor in certificate.vendors">
                                    ${ vendor }
                                </li>
                            </ul>
                        </td>
                        <td>
                            <a v-if="certificate.absolute_url" :href="certificate.absolute_url">
                                <i class="fa fa-download" title="{% trans 'Download' %}"></i>
                            </a>
                            <a v-if="certificate.absolute_url" :href="certificate.absolute_url + '?view=true'" target="_blank">
                                <i class="fa fa-search" title="{% trans 'View' %}"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div v-else class="row">
            <div v-for="certificate in certificates" class="col-md-3">
                <div class="card mb-4 text-center">
                    <div class="card-header">
                        <img class="card-img-top img-fluid rounded mx-auto d-block" :src="certificate.preview" alt="">
                    </div>
                    <div class="m-1 card-body">
                        <h6 class="card-subtitle mb-2 text-muted">${ certificate.type }</h6>
                        <h5 class="card-title">${ certificate.title }</h5>
                        <h6 class="small">${ certificate.updated }</h6>
                    </div>
                    <div class="card-footer">
                        <a :href="certificate.absolute_url" class="btn btn-warning">
                            <i class="fa fa-download" title="{% trans 'Download' %}"></i>
                        </a>
                        <a :href="certificate.absolute_url + '?view=true'" class="btn btn-warning" target="_blank">
                            <i class="fa fa-search" title="{% trans 'View' %}"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <certificate-pagination 
                :page="page"
                :max_page="max_page"
                v-on:nav-click="getNavigatePage"
            >
            </certificate-pagination>
        </div>
    </div>
    <script type="application/ld+json">
        [
            {
                "@context" : "http://schema.org",
                "@type" : "WebPage",
                "name" : "Useful materials",
                "description" : "Useful materials",
                "url" : "{{ fullsite }}{% url 'resource_view' %}",
                "image": {% include "begurucms/icon_jsonld.tpl" %},
                "primaryImageOfPage": {% include "begurucms/icon_jsonld.tpl" %},
                "copyrightYear" : "{% now "Y" %}",
                "copyrightHolder" : {% include "begurucms/copyright_jsonld.tpl" %},
                "publisher" : {% include "begurucms/copyright_jsonld.tpl" %}
            }
        ]
    </script>
{% endblock pagecontent %}
