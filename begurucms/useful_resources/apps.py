from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UsefulResourcesConfig(AppConfig):
    name = 'begurucms.useful_resources'
    verbose_name = _('Useful resources')

    def ready(self):
        from . import signals
