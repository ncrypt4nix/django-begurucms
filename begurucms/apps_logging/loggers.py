import logging


logger = logging.getLogger('external_requests')


def write_log_request(response):
    logger.info('URL: {url} [{method}]\n\t- Body({body}):\n\t- Response: {response}'.format(
        url=response.request.url,
        method=response.request.method,
        body=response.request.body,
        response=response.content,
    ))
