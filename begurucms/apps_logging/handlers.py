from django.utils.log import AdminEmailHandler
from django.core.mail import EmailMultiAlternatives
from django.conf import settings


class DevelopEmailHandler(AdminEmailHandler):
    """
    Send email notification about errors
    """
    def __init__(self, include_html=False, email_backend=None):
        """
        :param include_html:
        :param email_backend:
        """
        super(DevelopEmailHandler, self).__init__(
            include_html=include_html,
            email_backend=email_backend
        )
        self.mail_to = settings.LOG_DEV_MAIL_TO
        self.mail_from = settings.LOG_DEV_MAIL_FROM
        self.host = ''

    def emit(self, record):
        if not self.host:
            if hasattr(record, 'request'):
                self.host = record.request.META['HTTP_HOST']
        super(DevelopEmailHandler, self).emit(record)

    def send_mail(self, subject, message, html_message=None, *args, **kwargs):
        """
        sending mail to developers from developers_list
        :param subject:
        :param message:
        :param html_message
        :param args:
        :param kwargs:
        :return:
        """
        if ('Invalid HTTP_HOST header' in subject and
                'ERROR (EXTERNAL IP)' in subject):
            return
        if settings.DEBUG:
            return
        msg = EmailMultiAlternatives(
            '{}: {}'.format(self.host, subject),
            message,
            self.mail_from,
            self.mail_to
        )
        if html_message:
            msg.attach_alternative(html_message, "text/html")
        msg.send()
