import os
from django.apps import AppConfig
from django.conf import settings
from django.utils.log import configure_logging
from pathlib import Path


class AppsLoggingConfig(AppConfig):
    name = 'begurucms.apps_logging'

    def ready(self):
        """
        set initial settings
        """
        log_dir = (
            settings.LOG_PATH
            if hasattr(settings, 'LOG_PATH')
            else os.path.join(str(Path.home()), 'log')
        )
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        LOG_FILE = os.path.join(log_dir, 'django_logfile.log')
        EXTERNAL_REQUESTS_FILE = os.path.join(log_dir, 'external_requests.log')

        if not hasattr(settings, 'LOG_DEV_MAIL_TO'):
            settings.LOG_DEV_MAIL_TO = ['webmaster@tayle.ru', ]

        if not hasattr(settings, 'LOG_DEV_MAIL_FROM'):
            settings.LOG_DEV_MAIL_FROM = 'site@tayle.ru'

        django_handlers = ['console', 'log_file']

        settings.LOGGING = {
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': {
                'log_format': {
                    'format': '\n'
                              'TIME: %(asctime)s\n'
                              'LEVEL_NAME: %(levelname)s\n'
                              'MESSAGE: %(message)s\n'
                              'MODULE: %(module)s\n'
                              'FILE_NAME: %(filename)s\n'
                              'FUNC_NAME: %(funcName)s\n'
                              'PATH_NAME: %(pathname)s\n'
                },
            },
            'handlers': {
                'console': {
                    'level': 'DEBUG',
                    'class': 'logging.StreamHandler',
                },
                'log_file': {
                    'level': 'INFO',
                    'class': 'logging.FileHandler',
                    'filename': LOG_FILE,
                    'formatter': 'log_format',
                },
                'external_requests_handler': {
                    'level': 'INFO',
                    'class': 'logging.FileHandler',
                    'filename': EXTERNAL_REQUESTS_FILE
                },
                'mail_admins': {
                    'level': 'ERROR',
                    'class': 'begurucms.apps_logging.handlers.DevelopEmailHandler',
                    'formatter': 'log_format',
                    'include_html': True,
                },
            },
            'loggers': {
                'django.security.DisallowedHost': {
                    'handlers': [],
                    'propagate': False,
                },
                'django': {
                    'handlers': django_handlers,
                    'level': 'INFO',
                    'propagate': True,
                },
                'external_requests': {
                    'handlers': ['external_requests_handler'],
                    'level': 'INFO',
                    'propagate': True,
                },
            },
        }
        configure_logging(settings.LOGGING_CONFIG, settings.LOGGING)
