from django.template.backends.django import DjangoTemplates, Template
from django.template import TemplateDoesNotExist


class BGCMSTemplatesBackEnd(DjangoTemplates):
    """Add templates/global path to standard django backend"""
    def get_template(self, template_name):
        try:
            return Template(
                self.engine.get_template('/'.join(['global', template_name])),
                self
            )
        except TemplateDoesNotExist:
            return super().get_template(template_name)
