from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError


def rebuild_MPTT(modeladmin, request, queryset):
    """
    rebuild mptt model action instead of do it in python shell
    """
    if not request.user.is_superuser:
        modeladmin.message_user(request, _("You not superuser."))
        return
    modeladmin.model._tree_manager.rebuild()
    modeladmin.message_user(request, _("Rebuild done."))


rebuild_MPTT.short_description = _('rebuild this mptt model')


def duplicate_obj(modeladmin, request, queryset):
    def gen_unique_attr(old):
        return old + old
    new_obj_list = []
    for obj in queryset:
        new_obj = obj
        new_obj.pk = None
        if getattr(new_obj, 'ispublished', False):
            new_obj.ispublished = False
        if getattr(new_obj, 'title', None):
            new_obj.title = new_obj.title + '_copy'
        for i in obj._meta.fields:
            if i.unique:
                if i.primary_key:
                    continue
                new_attr = gen_unique_attr(getattr(new_obj, i.attname, None))
                while modeladmin.model.objects.filter(
                    **{i.attname: new_attr}
                ).count():
                    new_attr = gen_unique_attr(new_attr)
                setattr(new_obj, i.attname, new_attr)
                try:
                    new_obj.validate_unique()
                except ValidationError as error:
                    modeladmin.message_user(request, error.messages)
                    return
        new_obj_list.append(new_obj)
    unique_values = [
        i.attname
        for i in queryset.first()._meta.fields
        if i.unique and not i.primary_key
    ]
    modeladmin.model.objects.bulk_create(new_obj_list)
    modeladmin.message_user(
        request,
        _("Done. Please change: {unique_list}".format(
            unique_list=unique_values
        ))
    )


admin.site.add_action(duplicate_obj, _('duplicate objects'))
