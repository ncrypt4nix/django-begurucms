from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class ResultAndMaxPagePaginator(PageNumberPagination):
    page_size = 25
    page_size_query_param = 'page_size'

    def get_paginated_response(self, data):
        return Response({
            'results': data,
            'num_pages': self.page.paginator.num_pages
        })
