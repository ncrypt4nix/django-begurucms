from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
from django_summernote.views import SummernoteUploadAttachment
from django.core.urlresolvers import resolve
from django.contrib.contenttypes.models import ContentType


class SummernoteUploadAttachmentPlugin(SummernoteUploadAttachment):
    """Plugin integrated PublicationsContentFiles"""
    def post(self, request, *args, **kwargs):
        """add content info to file object"""
        from_url = request.META.get('HTTP_X_FROM_URL')
        model = resolve(from_url).func.model_admin.model
        content_type = ContentType.objects.get_for_model(model)
        try:
            object_id = int(resolve(from_url).args[0])
        except IndexError:  # just create new model obj
            return HttpResponse(
                _('Before you upload the picture, save the page'),
                status=500
            )
        post = request.POST.copy()
        post.update({
            'content_type': content_type,
            'object_id': object_id
        })
        request.POST = post
        return super().post(request, *args, **kwargs)
