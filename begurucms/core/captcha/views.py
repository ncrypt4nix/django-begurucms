import datetime
from django.conf import settings
from django.core.cache import cache
from django.views.generic.edit import FormView
from .forms import CaptchaForm


class CapchaObject:
    """Class for work with remote addr into cache"""
    if not hasattr(settings, 'CAPTCHA_DELTA'):
        CAPTCHA_DELTA = datetime.timedelta(seconds=1)
    else:
        CAPTCHA_DELTA = datetime.timedelta(seconds=settings.CAPTCHA_DELTA)
    if not hasattr(settings, 'CAPTCHA_LIFE'):
        CAPTCHA_LIFE = 24 * 60 * 60
    else:
        CAPTCHA_LIFE = settings.CAPTCHA_LIFE
    CACHE_KEY = 'captcha_key_'
    CACHE_PREFER = '_prefer'
    CACHE_BOT = '_bot'

    def __init__(self, obj_addr):
        self.full_obj_addr = self.CACHE_KEY + obj_addr
        self.prefer_obj_addr = self.full_obj_addr + self.CACHE_PREFER
        self.bot_obj_addr = self.full_obj_addr + self.CACHE_BOT
        self.obj_time = cache.get(self.full_obj_addr)
        super().__init__()

    @property
    def is_bot(self):
        """check if addr exist"""
        if not self.obj_time:
            return False
        bot_flag = cache.get(self.bot_obj_addr, None)
        if bot_flag is not None:
            return bot_flag
        delta_flag = self.obj_time > (
            datetime.datetime.utcnow() - self.CAPTCHA_DELTA
        )
        if delta_flag:
            self.is_bot = True
            return True
        return False

    @is_bot.setter
    def is_bot(self, flag):
        """set up is bot value"""
        cache.set(self.bot_obj_addr, flag, timeout=self.CAPTCHA_LIFE)

    def save(self):
        """save obj"""
        cache.set(
            self.full_obj_addr,
            datetime.datetime.utcnow(),
            timeout=self.CAPTCHA_LIFE
        )

    @property
    def page(self):
        return cache.get(self.prefer_obj_addr)

    @page.setter
    def page(self, page):
        cache.set(self.prefer_obj_addr, page, timeout=self.CAPTCHA_LIFE)

    def clear(self):
        cache.delete_many(
            [
                self.full_obj_addr,
                self.prefer_obj_addr,
                self.bot_obj_addr
            ]
        )


class CaptchaView(FormView):
    """Check in captcha view"""
    template_name = 'captcha/recaptcha.tpl'
    form_class = CaptchaForm

    def form_valid(self, form):
        obj = CapchaObject(self.request.META['REMOTE_ADDR'])
        self.success_url = obj.page
        obj.is_bot = False
        return super().form_valid(form)
