from django.conf.urls import url
from .views import CaptchaView

urlpatterns = [
    url(r'^captcha/$', CaptchaView.as_view(), name='captcha_redirected')
]
