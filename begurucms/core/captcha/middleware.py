from django.urls import reverse
from django.http.response import HttpResponse
from django.urls import resolve
from .views import CapchaObject


class CaptchaMiddleware:
    """Middleware for processing DOS attacks and spam attacks"""

    def process_view(self, request, view_func, view_args, view_kwargs):
        if request.method != 'POST':
            return None
        if (resolve(request.META.get('PATH_INFO')).app_name == 'admin' or
                request.user.is_authenticated):
            return None
        obj = CapchaObject(request.META['REMOTE_ADDR'])
        if obj.is_bot:
            capcha_url = reverse('captcha_redirected')
            if request.META.get('PATH_INFO') != capcha_url:
                obj.page = request.META.get('PATH_INFO')
                response = HttpResponse(
                    "<script>window.location.replace('{url}')</script>".format(
                        url=capcha_url
                    )
                )
                response['Ajax-Redirect'] = capcha_url
                return response
        obj.save()
        return None
