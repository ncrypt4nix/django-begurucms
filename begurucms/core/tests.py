from django.test import TestCase
from django.urls import reverse


class TestSiteMap(TestCase):
    """
    smoke tests app using sitemap
    """
    fixtures = ['all_data.json']

    def test_sitemap_on_200_response(self):
        """
        check responses in sitemaps on 200 status
        """
        sitemap_url = reverse('django.contrib.sitemaps.views.sitemap')
        response = self.client.get(sitemap_url)
        site_pages = [
            i['location'].replace('http://example.com/', '/')
            for i in response.context_data['urlset']
        ]
        for i in site_pages:
            if self.client.get(i).status_code != 404:
                self.assertEqual(self.client.get(i).status_code, 200)
