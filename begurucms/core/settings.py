import os
import re
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.redis import RedisIntegration


class CoreSettings:
    """general django settings for all sites"""
    file = __file__
    solr_core = 'core'
    debug = False
    solr_hostname = '127.0.0.1'
    redis_hostname = '127.0.0.1'


    @classmethod
    def set_debug(cls, debug):
        """set debug variable"""
        cls.debug = debug

    @classmethod
    def set_solr_hostname(cls, hostname):
        """set hostname to solr connection"""
        cls.solr_hostname = hostname

    @classmethod
    def set_redis_hostname(cls, hostname):
        """set hostname to solr connection"""
        cls.redis_hostname = hostname

    @classmethod
    def set_path(cls, file):
        """set path to settings.py file"""
        cls.file = file

    @classmethod
    def set_solr_core(cls, core):
        """set solr core name to connect to solr core"""
        cls.solr_core = core

    @classmethod
    def general_settings(cls):
        """get general settings"""
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(cls.file)))
        HOME_DIR = os.path.dirname(BASE_DIR)
        SRC = os.path.dirname(HOME_DIR)

        DEBUG = cls.debug
        PREPEND_WWW = not cls.debug
        RUNSERVER = not cls.debug
        WSGI_APPLICATION = 'cms.wsgi_run.application'
        ROOT_URLCONF = 'cms.urls_run'
        SITE_ID = 1
        LANGUAGE_CODE = 'ru-RU'
        TIME_ZONE = 'Europe/Moscow'
        USE_I18N = True
        USE_L10N = True
        USE_TZ = True
        STATIC_URL = '/static/'
        STATIC_ROOT = os.path.join(HOME_DIR, 'static')
        EXTERNAL_MEDIA_ROOT = HOME_DIR
        MEDIA_ROOT = os.path.join(EXTERNAL_MEDIA_ROOT, 'allavailable')
        MEDIA_URL = '/media/'
        SESSION_SERIALIZER = 'django.contrib.sessions.serializers.PickleSerializer'
        USE_CONTENT_FILES = ['catalog.product.both', ]
        NOCAPTCHA = True
        CACHES = {
            "default": {
                "BACKEND": "django_redis.cache.RedisCache",
                "LOCATION": "redis://{hostname}:6379/1".format(
                    hostname=cls.redis_hostname
                ),
                "OPTIONS": {
                    "CLIENT_CLASS": "django_redis.client.DefaultClient",
                }
            }
        }
        TRUSTED_IP = [
            '10.1.1.21'
        ]
        INSTALLED_APPS = [
            'django.contrib.admin',
            'django.contrib.sites',
            'django.contrib.sitemaps',
            'django.contrib.auth',
            'django.contrib.contenttypes',
            'django.contrib.sessions',
            'django.contrib.messages',
            'django.contrib.staticfiles',
            'django.contrib.humanize',
            'django_bootstrap_breadcrumbs',
            'dal',
            'dal_select2',
            'rest_framework',
            'rest_framework_swagger',
            'render_as_template',
            'subdomains',
            'mptt',
            'taggit',
            'sitetree',
            'haystack',
            'widget_tweaks',
            'logentry_admin',
            'captcha',
            'django_celery_beat',
            'django_celery_results',
            'corsheaders',
            'django_summernote',
            'begurucms.core',
            'begurucms.trackemail',
            'begurucms.pages',
            'begurucms.geo',
            'begurucms.catalog',
            'begurucms.events',
            'begurucms.optins',
            'begurucms.company',
            'begurucms.useful_resources',
            'begurucms.partner_client',
            'begurucms.vacancy',
            'begurucms.apps_logging',
            'begurucms.search',
            'begurucms.catalog__filters',
            'begurucms.api',
            'begurucms.utils',
            'begurucms.report',
        ]
        MIDDLEWARE_CLASSES = [
            'django.contrib.sessions.middleware.SessionMiddleware',
            'django.middleware.locale.LocaleMiddleware',
            'corsheaders.middleware.CorsMiddleware',
            'django.middleware.common.CommonMiddleware',
            'subdomains.middleware.SubdomainURLRoutingMiddleware',
            'begurucms.utils.middleware.SubdomainConfigMiddleware',
            'django.middleware.csrf.CsrfViewMiddleware',
            'django.contrib.auth.middleware.AuthenticationMiddleware',
            'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
            'django.contrib.messages.middleware.MessageMiddleware',
            'django.middleware.clickjacking.XFrameOptionsMiddleware',
            'django.middleware.security.SecurityMiddleware',
        ]
        SUBDOMAIN_URLCONFS = {
            None: 'cms.urls_run',
            'www': 'cms.urls_run',
            'www.loc': 'cms.urls_run',
            'www.eshop': 'cms.urls_eshop_run',
            'www.eshop.loc': 'cms.urls_eshop_run',
        }
        SUBDOMAIN_CONFIG = {
            None: {
                'base_template': 'base.tpl',
                'shortname': 'www',
                'suffix': ''
            },
            'www': {
                'base_template': 'base.tpl',
                'shortname': 'www',
                'suffix': ''
            },
            'www.loc': {
                'base_template': 'base.tpl',
                'shortname': 'www',
                'suffix': ''
            },
            'www.eshop': {
                'base_template': 'base_eshop.tpl',
                'shortname': 'eshop',
                'suffix': '_eshop'
            },
            'www.eshop.loc': {
                'base_template': 'base_eshop.tpl',
                'shortname': 'eshop',
                'suffix': '_eshop'
            },
        }
        TEMPLATES = [
            {
                'BACKEND': 'begurucms.core.backends.BGCMSTemplatesBackEnd',
                'DIRS': [
                    os.path.join(BASE_DIR, 'templates'),
                    'templates',
                ],
                'APP_DIRS': True,
                'OPTIONS': {
                    'context_processors': [
                        'django.template.context_processors.debug',
                        'django.template.context_processors.request',
                        'django.contrib.auth.context_processors.auth',
                        'django.contrib.messages.context_processors.messages',
                        'begurucms.utils.views.bgcms_processor',
                    ],
                },
            },
        ]
        REST_FRAMEWORK = {
            'DEFAULT_RENDERER_CLASSES': (
                'rest_framework.renderers.JSONRenderer',
                'rest_framework.renderers.BrowsableAPIRenderer',
            ),
        }
        HAYSTACK_CONNECTIONS = {
            'default': {
                'ENGINE': 'haystack.backends.solr_backend.SolrEngine',
                'URL': 'http://{hostname}:8983/solr/{core}'.format(
                    core=cls.solr_core,
                    hostname=cls.solr_hostname
                ),
                'ADMIN_URL': 'http://{hostname}:8983/solr/admin/cores'.format(
                    hostname=cls.solr_hostname
                ),
                'INCLUDE_SPELLING': True,
                'EXCLUDED_INDEXES': [
                    'begurucms.search.search_indexes.SectionMPTTIndex',
                    'begurucms.search.search_indexes.ContentFilesIndex'
                ]
            },
        }
        HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
        HAYSTACK_DEFAULT_OPERATOR = 'AND'
        HAYSTACK_FUZZY_MIN_SIM = 0.65
        REQUIRED_ICON = [
            'Product',
            'Publication',
            'SectionMPTT'
        ]
        CORS_ALLOW_METHODS = (
            'GET',
        )
        CORS_URLS_REGEX = r'^/api/.*$'
        CORS_ORIGIN_ALLOW_ALL = True
        # Celery
        CELERY_BROKER_URL = "redis://{hostname}:6379".format(
            hostname=cls.redis_hostname
        )
        CELERY_RESULT_BACKEND = "redis://{hostname}:6379".format(
            hostname=cls.redis_hostname
        )
        CELERY_ACCEPT_CONTENT = ['application/json']
        CELERY_TASK_SERIALIZER = 'json'
        CELERY_RESULT_SERIALIZER = 'json'
        CELERY_TIMEZONE = TIME_ZONE
        NOIMAGE_NAME = 'img/noimage.png'
        # Summernote
        SUMMERNOTE_THEME = 'bs4'
        SUMMERNOTE_CONFIG = {
            'attachment_model': 'pages.PublicationContentFiles',
            'summernote': {
                'width': '100%',
                'height': '480',
                'fontSizes': [
                    '8',
                    '9',
                    '10',
                    '11',
                    '12',
                    '14',
                    '16',
                    '18',
                    '22',
                    '24',
                    '36',
                    '48',
                    '64',
                    '82',
                    '150'
                ],
                'fontSize': 14,
            },
            'attachment_filesize_limit': 1024 * 1024 * 10,
        }
        return (
            lambda lcls=locals(), search=re.search:
            {i: lcls[i] for i in lcls if not search(r'^(cls|__.+__)$', i)}
        )()
