from django.conf.urls import include, url
from django.views.i18n import JavaScriptCatalog
from .plugins.views import SummernoteUploadAttachmentPlugin
handler500 = 'core.views.handler500'
summernote_urls = url(r'^summernote/', include('django_summernote.urls'))
for i in range(0, len(summernote_urls.urlconf_name.urlpatterns)):
    pattern = summernote_urls.urlconf_name.urlpatterns[i]
    if pattern.name == 'django_summernote-upload_attachment':
        pattern.callback = SummernoteUploadAttachmentPlugin.as_view()

urlpatterns = [
    summernote_urls,
    url('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),
]
