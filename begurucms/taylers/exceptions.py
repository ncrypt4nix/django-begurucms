class TaylerTransactionError(Exception):
    """Base exception for taylers"""
    pass


class BalanceCannotBeNegative(TaylerTransactionError):
    """Exception occurs when the balance becomes negative"""
    pass


class AmountOfLotsCannotBeZero(TaylerTransactionError):
    """ Exceptions occurs when amount of lots = 0.
        Because division on operations
    """
    pass


class TransactionCannotBeChanged(TaylerTransactionError):
    """Exception occurs when try change transaction or account employee"""
    pass


class ReverseTwiceError(TaylerTransactionError):
    """Raised by if try reverse transaction twice"""
    pass


class ReverseNotExistingTransaction(TaylerTransactionError):
    """Raised by if created transaction with reverse"""
    pass
