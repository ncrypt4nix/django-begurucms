from django.db.models import F, ExpressionWrapper, IntegerField
from django.views.generic.list import ListView
from begurucms.utils.views import BGCMSEmployeeViewMixin
from begurucms.company.taylers.models import TaylerAccount


class TaylersReportView(BGCMSEmployeeViewMixin, ListView):
    paginate_by = 40

    def get_queryset(self, *args, **kwargs):
        try:
            account = self.optin.employee.account
        except TaylerAccount.DoesNotExist:
            account = TaylerAccount.objects.create(
                employee=self.optin.employee
            )
        return account.transactions.all().annotate(
            cost_of_lot=ExpressionWrapper(
                F('total_sum') / F('amount_of_lots'),
                output_field=IntegerField()
            )
        )
