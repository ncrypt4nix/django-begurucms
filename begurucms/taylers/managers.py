from django.db import models


class TaylerManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().exclude(employee__isactive=False)
