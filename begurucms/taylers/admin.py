from django.contrib import admin
from django.contrib.admin.options import BaseInlineFormSet
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from begurucms.company.taylers.mixins import CleanedActionsAdmin
from begurucms.company.taylers.exceptions import (
    ReverseTwiceError,
    BalanceCannotBeNegative,
    TransactionCannotBeChanged
)
from begurucms.company.taylers.models import (
    TaylerTransaction,
    TaylerAccount,
    TaylerLot,
    TaylerLotGroup
)


class LimitModelFormset(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        _kwargs = {self.fk.name: kwargs['instance']}
        self.queryset = kwargs['queryset'].filter(**_kwargs)[:5]

class TaylerTransactionInline(admin.TabularInline):
    """show last 10 trasaction"""
    model = TaylerTransaction
    max_num = 0
    formset = LimitModelFormset

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields = TaylerTransactionAdmin.fields
        self.readonly_fields = self.fields


@admin.register(TaylerLotGroup)
class TaylerLotGroupAdmin(admin.ModelAdmin):
    fields = ('title',)


@admin.register(TaylerLot)
class TaylerLotAdmin(admin.ModelAdmin):
    fields = ('name', 'cost', 'group')
    list_display = ('name', 'cost')
    list_filter = ('group',)
    search_fields = ('name',)


@admin.register(TaylerAccount)
class TaylerAccountAdmin(CleanedActionsAdmin, admin.ModelAdmin):
    fields = ('employee', 'balance')
    list_display = ('employee', 'balance')
    readonly_fields = ('balance',)
    search_fields = (
        'employee__lastname',
        'employee__firstname',
        'employee__middlename'
    )
    inlines = [TaylerTransactionInline]


@admin.register(TaylerTransaction)
class TaylerTransactionAdmin(CleanedActionsAdmin, admin.ModelAdmin):
    fields = (
        'account',
        'lot',
        'date',
        'comment',
        'amount_of_lots',
        'total_sum',
        'reverse_date'
    )
    list_display = ('account', 'lot', 'total_sum', 'date', 'reverse_date')
    list_filter = ('lot',)
    search_fields = ('account__employee__lastname',)
    readonly_fields = ('date', 'total_sum', 'reverse_date')
    actions = ['reverse_transaction']

    def reverse_transaction(self, request, queryset):
        if 1 < queryset.count():
            self.message_user(
                request,
                _('Allowed one reverse transaction at a time')
            )
            return
        try:
            transaction = queryset.first()
            transaction.reverse_date = timezone.now()
            transaction.save()
            self.message_user(request, _('Done'))
        except ReverseTwiceError:
            self.message_user(request, _('Cannot reverse a transaction twice'))
        except BalanceCannotBeNegative:
            self.message_user(
                request,
                _(
                    'After reverse transaction balance became negative.'
                    'Balance cannot be negative'
                )
            )
        except TransactionCannotBeChanged:
            self.message_user(request, _('Transaction too old to reverse'))
    reverse_transaction.short_description = _('Reverse Transaction')

    def get_actions(self, request):
        actions = super().get_actions(request)
        if not request.user.is_superuser:
            del actions['reverse_transaction']
        return actions
