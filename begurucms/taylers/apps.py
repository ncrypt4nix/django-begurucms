from django.apps import AppConfig


class TaylersConfig(AppConfig):
    name = 'taylers'

    def ready(self):
        from . import signals
