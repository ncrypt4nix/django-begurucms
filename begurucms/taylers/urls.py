from django.conf.urls import url
from begurucms.company.taylers.views import TaylersReportView


urlpatterns = [
    url(
        r'^employee/taylers/account/me/$',
        TaylersReportView.as_view(),
        name="taylers_account"
    ),
]
