from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from begurucms.company.models import Employee
from begurucms.company.taylers.exceptions import (
    BalanceCannotBeNegative,
    AmountOfLotsCannotBeZero,
    TransactionCannotBeChanged,
    ReverseTwiceError,
    ReverseNotExistingTransaction
)
from begurucms.company.taylers.managers import TaylerManager


class TaylerLotGroup(models.Model):
    """Group of taylers"""
    title = models.CharField(_('Title'), max_length=64)

    class Meta:
        verbose_name = _('Group of tayler lot')
        verbose_name_plural = _('Groups of tayler lot')

    def __str__(self):
        return self.title


class TaylerLot(models.Model):
    """Lot for transactions"""
    name = models.CharField(_('Name'), max_length=64)
    cost = models.IntegerField(_('Price of the lot'))
    group = models.ForeignKey(
        TaylerLotGroup,
        on_delete=models.CASCADE,
        verbose_name=_('Lot Group')
    )

    class Meta:
        verbose_name = _('Tayler lot')
        verbose_name_plural = _('Tayler lots')

    def __str__(self):
        return '{} ({})'.format(self.name, self.cost)


class TaylerAccount(models.Model):
    """Employee Tayler Account"""
    employee = models.OneToOneField(
        Employee,
        on_delete=models.CASCADE,
        verbose_name=_('Employee'),
        related_name='account',
    )
    balance = models.PositiveIntegerField(_('Balance'), default=0)
    objects = TaylerManager()

    class Meta:
        verbose_name = _('Tayler Account')
        verbose_name_plural = _('Tayler Account')
        ordering = ['employee']

    def __str__(self):
        return f'{self.employee}'

    def save(self, *args, **kwargs):
        if self.id:
            kwargs['update_fields'] = ['balance']
        if self.balance < 0:
            raise BalanceCannotBeNegative
        super().save(*args, **kwargs)

    def clean(self):
        if not self.id:
            return super().clean()
        db_employee_id = TaylerAccount.objects.get(id=self.id).employee_id
        if db_employee_id != self.employee_id:
            raise ValidationError('You don\'t change employee')


class TaylerTransaction(models.Model):
    """Transaction to taylers account"""
    account = models.ForeignKey(
        TaylerAccount,
        on_delete=models.CASCADE,
        verbose_name=_('Account'),
        related_name='transactions'
    )
    lot = models.ForeignKey(
        TaylerLot,
        on_delete=models.CASCADE,
        verbose_name=_('Lot')
    )
    date = models.DateTimeField(_('Transaction date'), auto_now_add=True)
    comment = models.TextField(_('Comment to transaction'), blank=True)
    amount_of_lots = models.PositiveSmallIntegerField(
        _('Amount of lots'),
        default=1
    )
    total_sum = models.IntegerField(
        _('Total sum transaction'),
        blank=True,
        help_text=_(
            'Computed value. '
            'Do not enter if you do not want to override the default behavior'
        )
    )
    reverse_date = models.DateTimeField(
        _('Date to reverse transaction'),
        blank=True,
        null=True
    )

    class Meta:
        verbose_name = _('Tayler Transaction')
        verbose_name_plural = _('Tayler Transactions')
        ordering = ['-date']

    def __str__(self):
        return '{employee} - {lot} ({date})'.format(
            employee=self.account,
            lot=self.lot,
            date=self.date
        )

    @transaction.atomic
    def save(self, *args, **kwargs):
        """ Expect exceptions:
                BalanceCannotBeNegative
                TransactionCannotBeChanged
                AmountOfLotsCannotBeZero
                ReverseNotExistingTransaction
                ReverseTwiceError
        """
        if self.id and not self.reverse_date:
            raise TransactionCannotBeChanged
        if not self.amount_of_lots:
            raise AmountOfLotsCannotBeZero
        if self.reverse_date:
            if not self.id:
                raise ReverseNotExistingTransaction
            db_obj = TaylerTransaction.objects.get(id=self.id)
            if timezone.timedelta(days=30) < timezone.now() - db_obj.date:
                raise TransactionCannotBeChanged
            if db_obj.reverse_date:
                raise ReverseTwiceError
        else:
            self.total_sum = self.amount_of_lots * self.lot.cost
        super().save(*args, **kwargs)
        self.account.balance += (
            self.total_sum
            if not self.reverse_date
            else -db_obj.total_sum
        )
        self.account.save()

    def clean(self):
        if self.id:
            raise ValidationError(_('You don\'t change transaction'))
        if not self.amount_of_lots:
            raise ValidationError(_('Amount of a lot must not be zero'))
        if self.amount_of_lots * self.lot.cost < -self.account.balance:
            raise ValidationError(_('Balance cannot be negative'))
        return super().clean()
