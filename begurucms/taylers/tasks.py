import logging
from celery.task import task
from begurucms.company.models import Employee


logger = logging.getLogger('django')


@task
def send_email_about_change_balance(employee_id, transaction_id):
    """send email to employee about change your balance of taylers"""
    from begurucms.company.taylers.models import TaylerTransaction
    try:
        employee = Employee.objects.get(id=employee_id)
        transaction = employee.account.transactions.get(id=transaction_id)
        employee.send_modeled_email(
            tpl_name='balance_of_taylers_changed',
            transaction=transaction
        )
    except Employee.DoesNotExist:
        logger.error(
            'Try to send message to non exist employee '
            'about changing taylers balance.'
            'employee id = {}'.format(employee_id)
        )
    except TaylerTransaction.DoesNotExist:
        logger.error(
            'Try to send employee {employee} to message '
            'about not existing transaction.'
            'transaction id = {transaction_id}'.format(
                employee=employee,
                transaction_id=transaction_id
            )
        )
