from begurucms.company.taylers.models import TaylerAccount


class TaylersEmployeeMixin:
    """Mixin showing into context balance to current employee"""
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        try:
            context['tayler_balance'] = self.optin.employee.account.balance
        except TaylerAccount.DoesNotExist:
            context['tayler_balance'] = TaylerAccount.objects.create(
                employee=self.optin.employee
            )
        return context


class CleanedActionsAdmin:
    """Clear all already exist actions"""
    def get_actions(self, request):
        actions = super().get_actions(request)
        for action in actions.copy():
            if action not in self.actions:
                del actions[action]
        return actions
