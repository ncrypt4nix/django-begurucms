from django.dispatch import receiver
from django.db.models.signals import post_save
from begurucms.company.taylers.tasks import send_email_about_change_balance
from begurucms.company.taylers.models import TaylerTransaction


@receiver(post_save, sender=TaylerTransaction)
def balace_changed(sender, instance, **kwargs):
    send_email_about_change_balance.delay(
        instance.account.employee_id,
        instance.id
    )
