from django.apps import AppConfig


class PartnerClientConfig(AppConfig):
    name = 'begurucms.partner_client'
    verbose_name = 'Партнеры (client)'

    def ready(self):
        from begurucms.partner_client import signals
