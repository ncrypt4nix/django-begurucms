from django.db import models
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView
from django.db.models import Case, When, Sum
from django.utils.translation import ugettext as _
from rest_framework.renderers import JSONRenderer
from begurucms.catalog.models import Vendor
from begurucms.geo.models import City
from begurucms.geo.views import CitiesJsonView
from .forms import (
    PartnerOnCityForm,
    ContactEmailForm,
    ContactPhoneForm,
    PartnerRegisterForm
)
from begurucms.partner_client.models import (
    PartnerOnSiteRelated,
)
from begurucms.partner_client.serializers import (
    CitiesWithPartnersSerializer,
    PartnersFilterSerializer
)


class PartnerRegisterView(TemplateView):
    template_name = "partner_client/partner_register.tpl"
    OK_RESPONSE = _('We will contact you')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = PartnerRegisterForm()
        context['ok_response'] = self.OK_RESPONSE
        return context

    def post(self, request, *args, **kwargs):
        if not request.POST.get('create', None) == 'verify':
            return HttpResponse('Ok')
        form = PartnerRegisterForm(request.POST)
        if not form.is_valid():
            return JsonResponse({'code': 'error', 'data': str(form.errors)})
        obj = form.save()
        if 'vendors' in request.POST:
            obj.vendors.add(
                *list(
                    Vendor.objects.filter(
                        ident__in=request.POST.getlist('vendors')
                    )
                )
            )
        else:
            obj.vendors.add(*list(Vendor.objects.all()))
        obj.send_modeled_email(tpl_name='partner_register_form')
        obj.send_modeled_email(
            tpl_name='partner_register_form_back',
            back=True
        )
        obj.save()
        return HttpResponse(self.OK_RESPONSE)


class CityPartnersView(TemplateView):
    template_name = "partner_client/site_citypartners.tpl"
    city = None

    def dispatch(self, request, *args, **kwargs):
        self.city = get_object_or_404(City, pk=kwargs['cid'])
        return super(CityPartnersView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CityPartnersView, self).get_context_data(**kwargs)
        qs = PartnerOnSiteRelated.objects.filter(
            city=self.city,
            partner__ispublished=True
        ).select_related('partner', 'ptype').only(
            'partner__title',
            'partner__icon',
            'zipcode',
            'address',
            'phone',
            'email',
            'web',
            'ptype__name',
        ).distinct()
        context['partners'] = JSONRenderer().render(
            PartnersFilterSerializer(qs, many=True).data
        )
        context['city'] = self.city
        context['form'] = PartnerOnCityForm(initial={'city': self.city})
        context['cities'] = [i.data['label'] for i in context['form']['city']]
        return context


class ContactFormView(TemplateView):
    template_name = 'partner_client/contact.tpl'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['email_form'] = ContactEmailForm()
        context['phone_form'] = ContactPhoneForm()
        return context

    def post(self, request, *args, **kwargs):
        if not request.POST.get('create', None) == 'verify':
            return HttpResponse('Ok')
        if request.POST.get('email'):
            form = ContactEmailForm(request.POST)
            tpl_name = 'contact_form_int'
            back = True
        else:
            form = ContactPhoneForm(request.POST)
            tpl_name = 'contact_form_phone_int'
            back = False
        if not form.is_valid():
            return HttpResponse(_('Clean error'))
        obj = form.save()
        obj.send_modeled_email(tpl_name=tpl_name)
        if back:
            obj.send_modeled_email(tpl_name=tpl_name + "_back", back=True)
        return HttpResponse(_('We will contact you'))


class CitiesWithPartnersJsonView(CitiesJsonView):
    """
    View for getting JSON city data
    """
    serializer_class = CitiesWithPartnersSerializer

    def get_queryset(self, *args, **kwargs):
        """
        get cities with resale and installer count
        :param args:
        :param kwargs:
        :return: queryset
        """
        qset = super(CitiesWithPartnersJsonView, self).get_queryset(
            *args,
            **kwargs
        )
        known_cities = PartnerOnSiteRelated.objects.published().values_list(
            'city__pk',
            flat=True
        )
        qset = qset.filter(pk__in=known_cities)
        term = self.request.GET.get('term', None)
        if term is not None:
            qset = qset.filter(name__istartswith=term)
        return qset.annotate(
            installer_count=Sum(
                Case(
                    When(
                        partneronsiterelated__ptype__ident='installer',
                        then=1
                    ),
                    output_field=models.IntegerField()
                )
            )
        ).annotate(
            resale_count=Sum(
                Case(
                    When(partneronsiterelated__ptype__ident='resale', then=1),
                    output_field=models.IntegerField()
                )
            )
        ).only('name')
