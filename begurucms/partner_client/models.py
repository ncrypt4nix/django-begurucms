from phone_field import PhoneField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from begurucms.trackemail.models import (
    InternalMessageInterface,
    MessageInterface
)
from begurucms.utils.models import (
    AbstractModel,
)


class ContactFormAbstract(
    AbstractModel,
    MessageInterface,
    InternalMessageInterface
):
    """Abstract model form to feedback"""
    firstname = models.CharField(_('Name'), max_length=250)
    question = models.TextField(_('Question'))
    city = models.CharField(_('City'), max_length=250)

    class Meta:
        verbose_name = _('Visitor Question')
        verbose_name_plural = _('Visitor Questions')
        abstract = True


class ContactForm(ContactFormAbstract):
    """Model form to email feedback"""
    email = models.EmailField('Email', max_length=100)

    def send_modeled_email(self, tpl_name=None, back=False, **kwargs):
        message = self.get_email_message(tpl_ident=tpl_name)
        if (not message):
            return False
        if back:
            from_email = message.mdsettings.fromemail
            fromname = message.mdsettings.fromname
            to = self.email
            toname = '{0}'.format(self.firstname)
        else:
            from_email = self.email
            fromname = '{0}'.format(self.firstname)
            toname = ''
            if message.bccemail:
                to = message.bccemail
            else:
                to = message.mdsettings.fromemail
        self.send_full_email(
            from_email=from_email, to=to,
            from_name=fromname, to_name=toname,
            tpl_name=tpl_name,  **kwargs
        )

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = _('Visitor Email Question')
        verbose_name_plural = _('Visitor Email Questions')


class ContactFormPhone(ContactFormAbstract):
    """Model form to phone feedback"""
    phone = models.CharField(_('Phone'), max_length=30)

    def send_modeled_email(self, tpl_name=None, back=False, **kwargs):
        message = self.get_email_message(tpl_ident=tpl_name)
        if (not message):
            return False
        from_email = message.mdsettings.fromemail
        fromname = '{0}'.format(self.firstname)
        toname = ''
        if message.bccemail:
            to = message.bccemail
        else:
            to = message.mdsettings.fromemail
        self.send_full_email(
            from_email=from_email, to=to,
            from_name=fromname, to_name=toname,
            tpl_name=tpl_name,  **kwargs
        )

    def __str__(self):
        return self.phone

    class Meta:
        verbose_name = _('Visitor Phone Question')
        verbose_name_plural = _('Visitor Phone Questions')


class QuestionType(models.Model):
    """Question type model for VendorContactForm foreign key"""
    title = models.CharField(_('Title'), max_length=128)
    vendors = models.ManyToManyField(
        'catalog.Vendor',
        verbose_name=_('For vendor'),
    )

    def __str__(self):
        return self.title


class VendorContactForm(InternalMessageInterface, models.Model):
    """Any questions from users to vendor personal"""
    direction = models.ForeignKey(
        QuestionType,
        verbose_name=_('Direction'),
        on_delete=models.CASCADE
    )
    name = models.CharField(
        _('Full name'),
        max_length=120
    )
    email = models.EmailField(_('Email'))
    phone = PhoneField()
    company = models.CharField(
        _('Company'),
        max_length=128
    )
    description = models.TextField(_('Description'))
    vendor = models.ForeignKey(
        'catalog.Vendor',
        verbose_name=_('For vendor'),
        on_delete=models.CASCADE
    )

    def send_modeled_email(self, tpl_name=None, back=False, **kwargs):
        message = self.get_email_message(tpl_ident=tpl_name)
        if (not message):
            return False
        if back:
            if message.bccemail:
                from_email = message.bccemail
            else:
                from_email = message.mdsettings.fromemail
            fromname = message.mdsettings.fromname
            to = self.email
            toname = '{0}'.format(self.name)
        else:
            from_email = self.email
            fromname = '{0}'.format(self.name)
            toname = ''
            if message.bccemail:
                to = message.bccemail
            else:
                to = message.mdsettings.fromemail
        self.send_full_email(
            from_email=from_email, to=to,
            from_name=fromname, to_name=toname,
            tpl_name=tpl_name,  **kwargs
        )
