from django import forms
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
from captcha.fields import ReCaptchaField
from begurucms.geo.models import City
from .models import (
    PartnerTypeOnSite,
    ContactForm,
    ContactFormPhone,
    PartnerRegister
)


class PartnerOnCityForm(forms.Form):
    """
    Form for getting AJAX requests for filter
    by partners on city and by partner type
    """
    error_css_class = 'error'
    required_css_class = 'required'
    city = forms.ModelChoiceField(
        queryset=City.objects.all().filter(
            partneronsiterelated__ispublished=True
        ).only('name').distinct(),
        empty_label=None,
        label=_('City'),
        widget=forms.Select(
            attrs={
                'style': 'visibility: collapse;',
                'class': 'btn btn-default dropdown-toggle'
            }
        ),
    )
    ptype = forms.ModelChoiceField(
        # TODO remove this shit downstairs in queryset, please
        queryset=PartnerTypeOnSite.objects.all().only('name').filter(
            Q(ident='installer') | Q(ident='resale')
        ),
        empty_label=_("All"),
        label=_('Partner type'),
        widget=forms.Select(attrs={'class': 'form-control'}),
    )


class ContactEmailForm(forms.ModelForm):
    """Contat Email model form"""
    captcha = ReCaptchaField()

    class Meta:
        model = ContactForm
        fields = ['firstname', 'email', 'question', 'city', 'captcha']


class ContactPhoneForm(forms.ModelForm):
    """Contact Phone model form"""
    captcha = ReCaptchaField()

    class Meta:
        model = ContactFormPhone
        fields = ['firstname', 'question', 'phone', 'city', 'captcha']


class PartnerRegisterForm(forms.ModelForm):
    """From to register new partners"""
    captcha = ReCaptchaField()
    web = forms.URLField(
        widget=forms.TextInput,
        label=_('Web')
    )

    class Meta:
        model = PartnerRegister
        fields = [
            'firstname',
            'lastname',
            'occupation',
            'company',
            'web',
            'legal',
            'inn',
            'kpp',
            'cityname',
            'zipcode',
            'address',
            'phone',
            'mobile',
            'description',
            'email',
            'captcha'
        ]

    def clean_web(self):
        data = self.cleaned_data['web']
        if not data.startswith(('http', 'https')):
            data = 'http://' + data
        return data
