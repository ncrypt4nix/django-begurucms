import requests
from django.core.management.base import BaseCommand
from django.utils.translation import ugettext_lazy as _
from begurucms.partner_client.models import Server, PartnerTypeOnSite


class Command(BaseCommand):
    """ Sync model partner type with main server """
    help = "sync partner type"

    def handle(self, *args, **options):
        """
        run manage command
        :param args:
        :param options:
        :return: Succes
        """
        server = Server.objects.all()[0]
        ptype_request_list = requests.get(
            '{protocol}://{host}/api/getpartnertype/?format=json&'.format(
                protocol=server.protocol, host=server.host
            )
        ).json()
        ptype_created = 0
        for ptype in ptype_request_list:
            next, created = PartnerTypeOnSite.objects.update_or_create(
                id=ptype['id'],
                defaults=ptype
            )
            if created:
                ptype_created += 1
        print(_("Created: {created}").format(created=ptype_created))
        print(
            _("Updated: {updated}").format(
                updated=len(ptype_request_list) - ptype_created
            )
        )
