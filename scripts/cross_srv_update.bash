#!/bin/bash
#
# Update all sites including migrations, static, pip requirements
#

OPTS=`getopt -o hoc: --long help,command-before,command-begin,command-after,command-end,command-only,output,config: -n 'parse-options' -- "$@"`

if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi

HELP=false
COMMAND_BEGIN=false
COMMAND_BEFORE=false
COMMAND_AFTER=false
COMMAND_END=false
COMMAND_ONLY=false
OUTPUT="/dev/stdout"
CONFIG_PATH_DIR="$HOME/.config/cross_srv_update"
CONFIG_NAME="config"
SERVERS_FILE="$CONFIG_PATH_DIR/servers"

while true; do
  case "$1" in
    -h | --help )    HELP=true; shift ;;
    --command-begin ) COMMAND_BEGIN="$2"; shift; shift ;;
    --command-before ) COMMAND_BEFORE="$2"; shift; shift ;;
    --command-after ) COMMAND_AFTER="$2"; shift; shift ;;
    --command-end ) COMMAND_END="$2"; shift; shift ;;
    --command-only ) COMMAND_ONLY="$2"; shift; shift ;;
    -o | --output ) OUTPUT="$2"; shift; shift ;;
    -c | --config ) SERVERS_FILE="$2"; shift; shift ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

if $HELP
then
    echo "
-h --help: this page
--command-begin: command before pull repo
--command-before: command after pull repo but before other actions
--command-after: command before touch wsgi but after all updates and actions
--command-end: command after all actions
--command-only: only command without other actions on servers
-o --output: output messages. file, other thread or /dev/null 2>&1
-c --config: config file
"
exit
fi


if [ ! -d "$CONFIG_PATH_DIR" ]
then
    mkdir -p $CONFIG_PATH_DIR
fi

if [ ! -f "$CONFIG_PATH_DIR/$CONFIG_NAME" ]
then
    echo -n "Git username: "
    read GIT_USERNAME
    echo -n "Git password: "
    read GIT_PASSWORD
    touch "$CONFIG_PATH_DIR/$CONFIG_NAME"
    echo "USERNAME=$GIT_USERNAME" >> "$CONFIG_PATH_DIR/$CONFIG_NAME"
    echo "PASSWORD=$GIT_PASSWORD" >> "$CONFIG_PATH_DIR/$CONFIG_NAME"
fi

while read LINE
do
    DELIMITER=$(expr index "$LINE" =)
    VAR_NAME=${LINE:0:$DELIMITER - 1}
    if [ "$VAR_NAME" = "USERNAME" ]
    then
        GIT_USERNAME=${LINE:$DELIMITER}
    elif [ "$VAR_NAME" = "PASSWORD" ]
    then
        GIT_PASSWORD=${LINE:$DELIMITER}
    fi
done < "$CONFIG_PATH_DIR/$CONFIG_NAME"

if [ ! -f "$SERVERS_FILE" ]
then 
    echo "host;user;password;site_dir;git server name;git branch;django_settings;wsgi.py" > "$SERVERS_FILE"
    echo "change servers file: $SERVERS_FILE"
    echo "or use 'command -c servers_file_name'"
    exit
fi

if ! which 'sshpass' > /dev/null 2>&1
then
    echo 'Please, install sshpass'
    exit 1
fi
# initialization servers array
declare -a SERVERS
while read LINE
do
    SERVERS[${#SERVERS[*]}]="$LINE"
done < $SERVERS_FILE

for LINE in ${SERVERS[@]}
do
    arrIN=(${LINE//;/ }); i=0
    HOST="${arrIN[$i]}"; i=$i+1
    USER="${arrIN[$i]}"; i=$i+1
    PASSWORD="${arrIN[$i]}"; i=$i+1
    P_PATH="${arrIN[$i]}"; i=$i+1
    SERVER="${arrIN[$i]}"; i=$i+1
    BRANCH="${arrIN[$i]}"; i=$i+1
    REMOTE_DJANGO_SETTINGS="${arrIN[$i]}"; i=$i+1
    REMOTE_WSGI_DJANGO="${arrIN[$i]}"; i=$i+1

    echo "Next step: $USER@$HOST"

    if [ "$COMMAND_ONLY" != false ]
    then
        sshpass -p $PASSWORD ssh $USER@$HOST "cd ~/$P_PATH; $COMMAND_ONLY >> $OUTPUT"
        continue
    fi

    if [ "$COMMAND_BEGIN" != false ]
    then
        sshpass -p $PASSWORD ssh $USER@$HOST "cd ~/$P_PATH; $COMMAND_BEGIN >> $OUTPUT"
    fi

    # pull site
    EXPECT_COMMAND="set timeout -1; spawn git pull $SERVER $BRANCH; 
        expect \"Username\"; send \"$GIT_USERNAME\r\"; 
        expect \"Password\"; send \"$GIT_PASSWORD\r\"; 
        expect eof"
    sshpass -p $PASSWORD ssh $USER@$HOST "cd ~/$P_PATH/www; expect -c '$EXPECT_COMMAND' >> $OUTPUT"

    # pull bgcms
    sshpass -p $PASSWORD ssh $USER@$HOST "cd ~/$P_PATH/bgcms; expect -c '$EXPECT_COMMAND' >> $OUTPUT"

    if [ "$COMMAND_BEFORE" != false ]
    then
        sshpass -p $PASSWORD ssh $USER@$HOST "cd ~/$P_PATH; $COMMAND_BEFORE >> $OUTPUT"
    fi

    # requirements update
    sshpass -p $PASSWORD ssh $USER@$HOST "cd ~/$P_PATH/bgcms; pip3 install -r requirements.txt --user; >> $OUTPUT"

    # collect static
    EXPECT_COMMAND="set timeout -1; spawn python3 manage.py collectstatic --settings=$REMOTE_DJANGO_SETTINGS; 
        expect \"continue,\"; send \"yes\r\"; 
        expect eof"
    sshpass -p $PASSWORD ssh $USER@$HOST "cd ~/$P_PATH/www/cms; expect -c '$EXPECT_COMMAND' >> $OUTPUT"

    # install npm packages
    sshpass -p $PASSWORD ssh $USER@$HOST "npm install --prefix ~/$P_PATH/www/static >> $OUTPUT"

    # compile sass
    sshpass -p $PASSWORD ssh $USER@$HOST "cd ~/$P_PATH/www/static; sass --force --update --no-cache --style compressed sass:css >> $OUTPUT"

    # migrate
    sshpass -p $PASSWORD ssh $USER@$HOST "python3 ~/$P_PATH/www/cms/manage.py migrate --settings=$REMOTE_DJANGO_SETTINGS >> $OUTPUT"

    if [ "$COMMAND_AFTER" != false ]
    then
        sshpass -p $PASSWORD ssh $USER@$HOST "cd ~/$P_PATH; $COMMAND_AFTER >> $OUTPUT"
    fi

    # touch wsgi
    sshpass -p $PASSWORD ssh $USER@$HOST "touch ~/$P_PATH/www/cms/cms/$REMOTE_WSGI_DJANGO; >> $OUTPUT"
    
    if [ "$COMMAND_END" != false ]
    then
        sshpass -p $PASSWORD ssh $USER@$HOST "cd ~/$P_PATH; $COMMAND_END >> $OUTPUT"
    fi
done
exit 0
