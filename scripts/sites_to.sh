while getopts ":d:p:" optname
do
	case "$optname" in
		"d")
			ROOT=$OPTARG
			;;
		"p")
			MYSQL_PASS=$OPTARG
			;;
		":")
			echo "You forget value to arg"
			exit
			;;
		"?")
			echo "WFT? I dont know this arg!"
			exit
			;;
		*)
			echo "Something wrong. Please let me know."
			exit
			;;
	esac
done

if [ ! $ROOT ] || [ ! -d $ROOT ]
then
	while [ ! -d "$ROOT" ]
	do
		echo -n "Enter work directory: "
		read ROOT
	done
	CURRENT_PATH="$(pwd)"
	cd "$ROOT"
fi
if [ ! $MYSQL_PASS ]
then
	echo -n "Enter your MySQL root password: "
	read MYSQL_PASS
fi

BGCMS="git@techlab.tayle.ru:tayle/django-begurucms.git"
echo "Install sites in $ROOT"
while read SITE
do
	if [ ${SITE:0:1} = "#" ]
	then
		continue
	fi
	echo "Install $SITE"
	DIR=${SITE/*\//}
	DIR=${DIR/.git/}

    if [ $DIR = "avervcs" ]
    then
        DIR="avervcs-site"
    fi
	mkdir "$DIR"

	if [ $DIR = "ncomputingsu-site" ]
	then
		USITE="ncomputing"
	elif [ $DIR = "nikomaxru-site" ]
	then
		USITE="nikomax"
	elif [ $DIR = "gigalinkru-site" ]
	then
		USITE="gigalink"
	elif [ $DIR = "rextronsu-site" ]
	then
		USITE="rextron"
	elif [ $DIR = "tlkru-site" ]
	then
		USITE="tlk"
	elif [ $DIR = "nikomaxru-global" ]
	then
		USITE="nikomaxglobal"
	elif [ $DIR = "zorqru-site" ]
	then
		USITE="zorq"
	elif [ $DIR = "thecusru" ]
	then
		USITE="thecus"
		mkdir src
	else
		USITE=${DIR/-site/}
	fi
	cd "$DIR"
	
	pyvenv env
	source env/bin/activate
	pip install --upgrade pip
	if [ -d src ]
	then
		if [ $USITE = "thecus" ]
		then
			cd src/www
			git pull
			cd ..
        else
			cd src
			git pull
		fi
	else
		if [ $USITE = "thecus" ]
		then
			mkdir src
			git clone $SITE src/www
		else
			git clone $SITE src
		fi
		cd src
	fi

	if [ -d bgcms ]
	then
		cd bgcms
		git pull
		cd ..
	else
		git clone $BGCMS bgcms
	fi
	if [ $USITE = "thecus" ]
	then
		ln -s $(find . -name requirements.dev) requirements.prod
	fi
    REQUIREMENTS="bgcms/requirements.prod"
    if [ ! -f $REQUIREMENTS ]
    then
        REQUIREMENTS=$(find . -name requirements.prod && find . -name requirements.txt)
    fi
	pip install -r $REQUIREMENTS
	pip install django-debug-toolbar
    pip install pudb
    pip install ipdb
    pip install neovim

	if [ ! -d www/local_static ]
	then
		echo "Copy static"
		mkdir -p "www/local_static"
		echo "Download static"
		sshpass -p u$USITE scp -r u$USITE@10.1.1.21:$DIR/www/static/pkg $ROOT/$DIR/src/www/static/
		sshpass -p u$USITE scp -r u$USITE@10.1.1.21:$DIR/www/static/mdb $ROOT/$DIR/src/www/static/
		cd "www/local_static"
		ln -s "../static"
		cd -
	fi
    DUMP=$(echo "$USITE-$(date +%Y-%m-%d).sql")
	if [ ! -f "~/Downloads/$DUMP" ]
	then
        echo "Download dump"
        MYSQL_DATABASE_NAME=`cat www/cms/cms/settings_run.py | grep "'NAME'" | grep "$USITE"`
        MYSQL_DATABASE_NAME=${MYSQL_DATABASE_NAME#*:}
        MYSQL_DATABASE_NAME=${MYSQL_DATABASE_NAME%,*}
        sshpass -p u$USITE ssh u$USITE@10.1.1.21 "mysqldump -u u$USITE -pu$USITE $MYSQL_DATABASE_NAME > ~/dumps/$DUMP"
		sshpass -p u$USITE scp u$USITE@10.1.1.21:dumps/$DUMP ~/Downloads/$DUMP
        echo "Connection to MySQL"
        mysql -u root -p$MYSQL_PASS -e "GRANT ALL PRIVILEGES ON *.* TO 'u$USITE'@'localhost' IDENTIFIED BY 'u$USITE';"
        mysql -u u$USITE -pu$USITE -e "CREATE DATABASE $USITE;"
        gzip -d ~/Downloads/$DUMP
        mysql -u u$USITE -pu$USITE $USITE < ~/Downloads/${DUMP/.gz/}
	fi
	LOC_SET="www/cms/cms/local_settings.py"
	if [ ! -f $LOC_SET ]
	then
		touch $LOC_SET

		# write in local_settings.py
		echo "import sys" >> $LOC_SET
		echo "from .settings_run import *" >> $LOC_SET
		echo "from django.contrib.staticfiles.urls import staticfiles_urlpatterns" >> $LOC_SET
		echo >> $LOC_SET
		echo "sys.path.append('$ROOT/$DIR/src/bgcms')" >> $LOC_SET
		echo >> $LOC_SET
		echo "ALLOWED_HOSTS = ['*']">> $LOC_SET
		echo "DEBUG = True" >> $LOC_SET
		echo "PREPEND_WWW = False" >> $LOC_SET
		echo >> $LOC_SET
		echo "DATABASES = {" >> $LOC_SET
		echo "    'default': {" >> $LOC_SET
		echo "        'ENGINE': 'django.db.backends.mysql'," >> $LOC_SET
		echo "        'NAME': '$USITE'," >> $LOC_SET
		echo "        'USER': 'u$USITE'," >> $LOC_SET
		echo "        'PASSWORD': 'u$USITE'," >> $LOC_SET
		echo "        'HOST': '127.0.0.1'," >> $LOC_SET
		echo "        'PORT': ''," >> $LOC_SET
		echo "    }" >> $LOC_SET
		echo "}" >> $LOC_SET
		echo >> $LOC_SET
		echo "ROOT_URLCONF = 'cms.local_urls'" >> $LOC_SET
		echo "STATICFILES_DIRS = (os.path.join(HOME_DIR + '/local_static', 'static'),)" >> $LOC_SET
	fi

	LOC_URL="www/cms/cms/local_urls.py"
	if [ ! -f $LOC_URL ]
	then
		# write in local_urls.py
		echo "from django.contrib.staticfiles.urls import staticfiles_urlpatterns" >> $LOC_URL
		if [ -f "www/cms/cms/urls.py" ]
		then
			echo "from .urls import *" >> $LOC_URL
		elif [ -f "www/cms/cms/urls_run.py" ]
		then
			echo "from .urls_run import *" >> $LOC_URL
		fi
		echo  >> $LOC_URL
		echo  >> $LOC_URL

		echo "urlpatterns += staticfiles_urlpatterns()" >> $LOC_URL
	fi
	
	# add to local files in git exclude
	echo "Adding to git exclude"
	if [ "$USITE" = "thecus" ]
	then
		EXCLUDE="www/.git/info/exclude"
	else
		EXCLUDE=".git/info/exclude"
	fi
	if ! grep "www/cms/cms/local_settings.py" $EXCLUDE
	then
		echo "www/cms/cms/local_settings.py" >> $EXCLUDE
	fi
	if ! grep "www/cms/cms/local_urls.py" $EXCLUDE
	then
		echo "www/cms/cms/local_urls.py" >> $EXCLUDE
	fi
	if ! grep "www/local_static/" $EXCLUDE
	then
		echo "www/local_static/" >> $EXCLUDE
	fi
	python3 www/cms/manage.py migrate --settings=cms.local_settings
	deactivate
done < $CURRENT_PATH/sites.txt
