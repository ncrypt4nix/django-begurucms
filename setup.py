import os
from setuptools import setup, find_packages

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

with open(os.path.join(os.path.dirname(__file__), 'requirements.txt')) as r:
    requirements = r.read().splitlines()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-begurucms',
    version='1.0.0',
    packages=find_packages(),
    install_requires=requirements,
    dependency_links=[
        'git+https://techlab.tayle.ru/tayle/django-json-field-fork.git@master',
    ],
    include_package_data=True,
    license='BSD License',  # example license
    description='BeGURU CMS base system.',
    long_description=README,
    url='http://www.beguru.org/soft/',
    author='Igor Belousov',
    author_email='ibelousov@beguru.org',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        # Replace these appropriately if you are stuck on Python 2.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
